* File: INFEAS.RUL
* RULE FILE FOR INTERPRT INFEAS

$IF %STATUS(1:6) = INFEAS THEN GOTO BEGIN
%PROBLEM is feasible.
$EXIT

:BEGIN
$IF %SWSYN = F THEN ANALYZE READIN SYNTAX
$IF %SWSYN = F THEN GOTO ROWS
 Look at the schema table to see if anything looks peculiar.
$ANALYZE SCHEMA DISP TABL
$IF %SWOTFIL = F THEN ANALYZE _PAUSE

:ROWS
$ANALYZE SUBMAT ROW * S=I N=0
$IF %NROWS = 0 THEN SKIP 3
The following infeasible rows are null.
$ANALYZE D R
$EXIT

$ANALYZE SUB ROW * S=I
$SET NRINF = %NROWS
$IF %%NRINF = 0 THEN GOTO COLS
$IF %%NRINF = 1 THEN SKIP 2
 The following rows are infeasible
$SKIP 1
 The following row is infeasible.
$ANALYZE LIST * S=I,*

:CHKROW
$SET ROW=%ROW
$ANALYZE SUB ROW %ROW
$ANALYZE SUB COL *
$ANALYZE SUB SET
$SET LO = %VROWLO
$SET UP = %VROWUP
$SET MIN=0
$SET MAX=0

$LOOP
$SET XL=%VCOLLO
$SET XU=%VCOLUP
$LOOKUP NZ,%ROW,%COLUMN
$SET COEF=%VLOOK
$IF %%COEF > 0 THEN GOTO UPDATE
$CALC COEF = 0 - %%COEF
$SET X = %%XL
$CALC XL = 0 - %%XU
$CALC XU = 0 - %%X

:UPDATE
$IF %%MIN = -* THEN GOTO MAX
$IF %%XL = -* THEN GOTO INFMIN
$CALC MIN = MIN + %%XL * %%COEF
$GOTO MAX
:INFMIN
$SET MIN = -*
:MAX
$IF %%MAX = * THEN SKIP LOOP
$IF %%XU = * THEN GOTO INFMAX
$CALC MAX = MAX + %%XU * %%COEF
$SKIP LOOP
:INFMAX
$SET MAX = *

$IF %%MIN <> -* THEN SKIP LOOP
$IF %%MAX = * THEN GOTO COLS
$NEXT COL

$IF %%MIN <= %%UP THEN GOTO MAX
Row %%ROW is required to be <= %%UP, but the bounds on its activities
imply it must be >= %%MIN .
$EXIT
:MAX
$IF %%MAX >= %%LO THEN GOTO CHKRATE
Row %%ROW is required to be >= %%LO, but the bounds on its activities
imply it must be <= %%MAX .
$EXIT

:CHKRATE
$VECTOR RATE ROW %%ROW
$SET MIN = %VECTOR(-1)
$SET MAX = %VECTOR(0)
$IF %%MIN > %%UP THEN SKIP 1
$IF %%MAX >= %%LO THEN GOTO COLS
The system of equations imply equation %%ROW has the following alternative
form, which reveals that its range cannot fulfill its constraint.
That is, note that
$IF %%MIN > %%UP THEN SKIP 2
its level must be <= %%MAX, but it is required to be >= %%LO .
$SKIP 1
its level must be >= %%MIN, but it is required to be <= %%UP .
$TEXT
$IF %SWOTFIL = F THEN ANALYZE _PAUSE
$ANALYZE RATE ROW %%ROW //X
$EXIT

:COLS
$ANALYZE SUBMAT COL * S=I N=0
$IF %NCOLS = 0 THEN SKIP 3
The following infeasible columns are null.
$ANALYZE D C
$EXIT

$ANALYZE SUB COL * S=I
$SET NCINF = %NCOLS
$IF %%NCINF = 0 THEN GOTO ADVICE
$IF %%NCINF = 1 THEN SKIP 2
 The following columns are infeasible
$SKIP 1
 The following column is infeasible.
$ANALYZE DISP COL
$SET COL= %COLUMN
$SET XL = %VCOLLO
$SET XU = %VCOLUP
$VECTOR RATE COL %%COL
$SET MIN = %VECTOR(-1)
$SET MAX = %VECTOR(0)
$IF %%MIN > %%XU THEN SKIP 1
$IF %%MAX > %%XL THEN GOTO ADVICE
The system of equations imply that column %%COL must satisfy the
$IF %NVECTOR > 0 THEN GOTO EQ

null equation:  %%COL = 0.  This violates its
$IF %%XL <= 0 THEN SKIP 2
lower bound:  %%COL >= %%XL .
$EXIT
upper bound:  %%COL <= %%XU .
$EXIT

:EQ
following equation.
$TEXT 1 MARGIN=5
 %%COL =
$SET I=1
$LOOP
:LOOP
$SET RATE = %VECTOR(%%I)
$IF %%RATE < 0 THEN SKIP 1
 +
   %%RATE * %_NAME(%%I)
$IF %%I = %NVECTOR THEN ENDLOOP
$CALC I = I + 1
$GOTO LOOP
$ENDLOOP

$TEXT 1 MARGIN=1

This equation, and the associated bounds, imply the level of %%COL must be
$IF %%MIN <= %%XU THEN SKIP 2
 >= %%MIN, but this violates the bound:  %%COL <= %%XU .
$EXIT
 <= %%MAX, but this violates the bound:  %%COL >= %%XL .
$EXIT

:ADVICE
$TEXT
 I suggest you proceed with the following steps, as needed.
$TEXT 1 MARGIN=5
1. Look at rates of substitution (use RATEOF command).
$TEXT
2. Use the AGGREGAT command (AGGREG ROW *).
$TEXT
3. EXEC FORMIS and look at the constraints involved.
$TEXT
4. REDUCE *,* (first, enter OUTPUT %PROBLEM .RED, or some output file).
$TEXT
5. Obtain an IIS (from Chinneck''s code, MINOS(IIS)) and read it in
   (READ IIS %PROBLEM ).
$TEXT 1 MARGIN=1
 See, also, rule file INFABP.RUL, which you can instantiate with
$TEXT
 INTERP TEST INFABP {A | B | P | filespec}.
$TEXT
 ....................:...:...:...:...IIS file (not named A, B or P)
$TEXT
 ....................:...:...:...Uses row with max Phase I price
$TEXT
 ....................:...:...Uses blocks
$TEXT
 ....................:...Uses AGGREGAT command
$TEXT

$EXIT
