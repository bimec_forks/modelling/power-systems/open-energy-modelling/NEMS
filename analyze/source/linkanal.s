# script link ANALYZE

link /map:analyze.map /out:analyze.exe \
    flipmain.obj flipmgt.obj fldictry.obj flfiles.obj flkeywrd.obj \
    flparse.obj  flrand.obj  flscreen.obj flstring.obj\
    gbagnda1.obj gbagnda2.obj gbrange.obj gbrver.obj getedit.obj getiomat.obj\
    getiopck.obj getiosol.obj getiosub.obj getlib.obj getrans.obj getset.obj\
    gredbin.obj  gredmat.obj gredexp.obj gredexp2.obj greduce.obj gredtest.obj\
    analsys.obj  abasis.obj  aquery1.obj aquery2.obj arate1.obj arate2.obj\
    aedit.obj    areadin.obj aschema.obj asensitv.obj ashow.obj asubmat.obj \
    blkfind.obj  blkio.obj   blkmgt.obj  blkplot.obj\
    explain.obj  rbgenerl.obj rbcmnd1.obj rbcmnd2.obj rbstacks.obj\
    graphdo.obj  graphio.obj graphsyn.obj\
    schema.obj   scheqn.obj  screen.obj   scrplots.obj table.obj
