                       ANALYZE INTERPRT Command Options

    D...........Gives reduced cost interpretation for nonbasic columns.
                Parameters:  column names
                (if not specified, submatrix columns are used)
    FIND........Finds ROW, COL, SET or ELEMENT of a set, given string
                to match meaning.
                Parameter 1: ROW | COL | SET | ELEMENT
                Parameter 2 if 1 is ELEMENT: set name | *
                Remaining parameters:  string to match meaning
    INFEAS......Gives advice for diagnosing an infeasible LP.
                No parameters.
    MODEL.......Gives model interpretation (syntax required), including
                finding a maximal embedded (generalized) network.
                No parameters.
    PRICE.......Attempts price interpretation of one row for optimal LP
                The attempt could fail, depending upon the LP structure
                and how complicated the dual price is related to other
                prices.
                Parameter: row name
                (if not specified, submatrix row is used)
    OBJECTIV....Interprets the objective row.
                No parameters.
    REDUND......Interprets redundancies for submatrix rows and columns.
                No parameters.
    STATS.......Gives basic statistics of LP (beyond SUMMARY command
                and without LP jargon).
                No parameters.
    TEST........Used to test rules and experiment with new ones.
                Parameters: name of rule file to be instantiated, followed
                           by other parameters used by that rule file.
                (if not specified, a prepared test of rules is instantiated)
    UNBOUND.....Diagnosis of an unbounded LP.
                No parameters.
    WHATIF......Gives effect of changing bound(s).
                Parameter  1:   L | U
                Parameter  2:   ROW | COL
                Parameter  3:   name | SUBMAT | *  (* same as SUBMAT)
                Parameters 4-7: value | [value] w [ +|- value]
                                               :.:.:...:...note spaces
                                                :...w = L|U (same as Param 1)
    See Primer for further information.
