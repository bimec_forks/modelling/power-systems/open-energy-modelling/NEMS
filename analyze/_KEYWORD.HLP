    Function: List or change keywords
    Syntax:   _KEYWORD [{ CLEAR | {LOAD | MERGE} filespec
                        | WRITE [EXCEPT char]
                        | SET keyword = value [,...] }]

    Absent the command, the keys are displayed (to OUTPUT file).

    The keyword specifications are:
       CLEAR.....clears keywords
       LOAD .....loads new keywords (discarding old ones)
       MERGE.....merges new keywords with old ones
       SET  .....sets value of key
       WRITE.....writes key values

    Following LOAD and MERGE is the file specification (filespec)
    ...use _SETUP to list or change prefix and suffix.

    Following SET is the name of the key, as:

                  SET keyword = value [,...]

    If the value is *, the key is set to its default value.
    The [,...] indicates that more than 1 key can be set at one time.

                               ______
      ________________________| NOTE |_____________________________
     |                                                             |
     |  SET looks for all matches of the keyword.  If a name       |
     |  abbreviation matches more than 1, all key values for which |
     |  the value is admissible are updated by the specification.  |
     |_____________________________________________________________|

    The WRITE command puts some comments (* in column 1), including
    the keys at their default settings.  Those not at default settings
    are written in the form:      key  value

    There is an option to substitute a special character with a blank.
    (This is because the interactive session uses the blank as a delimeter
    so key names may have a special character, like _, to keep 1 key
    with 2 parts.   For example,

    _KEYWORD WRITE EXCEPT _
    ...writes the keys (to the OUTPUT file), except the underscore
       character (_) is replaced by a blank wherever it appears.

    Examples:

    _KEY
    ...displays all keys

    _K DISPLAY M
    ...displays all keys that begin with M

    _K DIS SAV
    ...displays all keys that begin with SAV

    _KEY CL
    ...clears the keys (from memory)

    _KEY S DAR .5
    ...sets keys that begin with DAR to .5 (if admissible)

    _KEY S DAR *
    ...restores keys that begin with DAR to their default values

    _KEY S * *
    ...restores all keys to their default values

    _KEY S *
    ...restores all keys to their default values

    _KEY L MYFILE.KEY
    ...loads the keys from MYFILE.KEY (using default prefix)

    _KEY M MYFILE
    ...merges the keys from MYFILE (using default prefix and suffix)

    _K SET EPSILON=.005
    ...sets all keys whose name matches EPSILON to a value of .005

    _K S EPS=.005 PARM2=YES PARM5
    ...sets all keywords that match EPS (such as EPSILON) to .005;
       sets PARM2 to YES (note:  keys can have numeric or non-numeric
       values);  and, sets PARM5 to its default value.

    OUTPUT MYFILE.DAT
    _K WR
    ...writes the key values to MYFILE.DAT
