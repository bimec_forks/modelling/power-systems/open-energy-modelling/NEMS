$Header: N:/default/scripts/RCS/graphlist.txt,v 1.19 2005/06/14 17:05:28 DSA Exp $
Graphs compatible with Ftab Layout File version:  layout.v1.131.txt
This file stores users' customized graph sets.
It is used by the program "combine_glf" to generate a graph layout file for Fgraph.

COMPARE: multi-run line graphs for comparing a Single Series across multiple runs
ONE RUN: Single-run graphs for within run comparisons and regional graphs
                                                     |<==Character needed to locate file names 
Select  Type                                         |   in this column. Do not remove.
1:yes    of                  Description             | Available Graph Layout File Sets
0:no   Graphs                                        | in N:\default\fgraph\GLFs\
====== =======  =====================================| =================================
1      COMPARE  Summary -- Top 12 Graphs             |SS_Top12_061005.glf
1      COMPARE  Table 1 Total Supply and Dispostion  |SS_Tab1_061005.glf
1      COMPARE  Table 2 Consumption by Sector, Fuel  |SS_Tab2_061005.glf
1      COMPARE  Table 3 Prices by Sector, Fuel       |SS_Tab3_061005.glf
0      COMPARE  Table 4 Residential                  |SS_Tab4_061005.glf
0      COMPARE  Table 5 Commercial                   |SS_Tab5_061005.glf
1      COMPARE  Table 6 Industrial                   |SS_Tab6_061005.glf
0      COMPARE  End Use Combined Heat and Power      |SS_CHPbyIndustry_061005.glf
0      COMPARE  Table 34 Shipment Value by Industry  |SS_Tab34_061005.glf
0      COMPARE  Table 38 Chemical Industry           |SS_Tab38_061005.glf
1      COMPARE  Table 7 Transportation               |SS_Tab7_061005.glf
1      COMPARE  Table 8 Elec Gen, Sales, Prices      |SS_Tab8_061005.glf
1      COMPARE  Table 9 Elec Capacity                |SS_Tab9_061005.glf
0      COMPARE  Table 62 Electric Power Projections  |SS_Tab62_061005.glf
0      COMPARE  Table 62 Electric Power REGIONAL     |SS_Tab62_061005_reg.glf
1      COMPARE  Table 11 Oil Supply/Disposition      |SS_Tab11_061005.glf
1      COMPARE  Table 12 Oil Product Prices          |SS_Tab12_061005.glf
1      COMPARE  Table 13 Natural Gas Supply/Disp     |SS_Tab13_061005.glf
1      COMPARE  Table 14 Oil and Gas Supply          |SS_Tab14_061005.glf
1      COMPARE  Table 15 Coal Supply/Disposition     |SS_Tab15_061005.glf
1      COMPARE  Table 16 Renewable Energy            |SS_Tab16_061005.glf
1      COMPARE  Table 17 Carbon Dioxide              |SS_Tab17_061005.glf
1      COMPARE  Table 18 Macroeconomic Indicators    |SS_Tab18_061005.glf
1      COMPARE  Table 19 International Oil Supply    |SS_Tab19_061005.glf
1      COMPARE  Table 48 Light Duty Sales by Tech    |SS_Tab48_061005.glf
0      COMPARE  Table 48 Light Duty Sales REGIONAL   |SS_Tab48_061005_reg.glf
1      COMPARE  Table 117 National CAA Impacts       |SS_Tab117_061005.glf
0      COMPARE  Table 70 Petroleum Price Components  |SS_Tab70_061005.glf
