XACTCASE,  8, 4,         
XACTDD  ,  8, 4,         
XACTERR ,  4, 5, 8
XACTFILE,  8, 4, ACTFILE 
XACTPROB,  8, 4, ACTPROB 
XBOUND  ,  8, 4, *       
XCCODE  ,  4, 1, 0
XCHAR01 ,  8, 4,         
XCHAR02 ,  8, 4,         
XCHAR03 ,  8, 4,         
XCHAR04 ,  8, 4,         
XCHAR05 ,  8, 4,         
XCHAR06 ,  8, 4,         
XCHAR07 ,  8, 4,         
XCHAR08 ,  8, 4,         
XCHAR09 ,  8, 4,         
XCHAR10 ,  8, 4,         
XCHECKSW,  4, 1, 0
XCHOBJ  ,  8, 4,         
XCHRHS  ,  8, 4,         
XCLOCKSW,  4, 1, 0
XCMASK  ,  8, 4, 
XCOMPERR,  4, 5, 9
XCOMPSW ,  4, 1, 0
XCRASHSW,  4, 1, 8
XCYCLESW,  4, 1, 32
XDATA   ,  8, 4, *
XDEGEND ,  4, 1, 2000
XDEGMAX ,  4, 1, 50
XDELTM  ,  4, 1, 0
XDEMAND ,  4, 5, 0
XDETAIL ,  4, 1, 0
XDFPGFIL,  8, 4, ACTPROG 
XDFPROG ,  8, 4, DFPROG  
XDOATTN ,  4, 5, 1
XDODELTM,  4, 5, 2
XDOFEAS ,  4, 5, 3
XDOFREQ1,  4, 5, 15
XDONFS  ,  4, 5, 4
XDOPRIM ,  4, 5, 5
XDOSLOPT,  4, 5, 6
XDOUNB  ,  4, 5, 7
XDUAL   ,  4, 1, 0
XEPS    ,  8, 3, 0.0001
XEQUIL  ,  4, 1, 1
XFEAS   ,  4, 1, 0
XFREE   ,  4, 1, 100
XFREQINV,  4, 1, 100
XFREQLOG,  4, 1, 100
XFREQSAV,  4, 1, 0
XFREQSUM,  4, 1, 100
XFUNCT  ,  8, 3, 0
XINSERT ,  8, 4, 
XINT01  ,  4, 1, 0
XINT02  ,  4, 1, 0
XINT03  ,  4, 1, 0
XINT04  ,  4, 1, 0
XINT05  ,  4, 1, 0
XINT06  ,  4, 1, 0
XINT07  ,  4, 1, 0
XINT08  ,  4, 1, 0
XINT09  ,  4, 1, 0
XINT10  ,  4, 1, 0
XINTGOAL,  8, 3, 0
XINVERT ,  4, 1, 0
XIOERR  ,  4, 5, 10
XITERNO ,  4, 1, 0
XJ      ,  4, 1, 0
XLOADB  ,  8, 4,         
XLUDENSE,  4, 1, 1500
XLUFILL ,  4, 1, 40
XLUINV  ,  4, 1, 1      
XM      ,  4, 1, 0
XMAJERR ,  4, 5, 11
XMAXITER,  4, 1, -1
XMAXPASS,  4, 1, 15
XMAXTIME,  4, 1, 0
XMAXVAR ,  8, 3, 1
XMAXW   ,  4, 1, 0
XMINERR ,  4, 5, 12
XMINMAX ,  8, 4, MIN     
XNEGDJ  ,  4, 1, 0
XNET    ,  4, 6, 0x00000000
XNIF    ,  4, 1, 0
XNOWT   ,  4, 1, 0
XOBJ    ,  8, 4, *       
XPARAM  ,  8, 3, 0
XPENCOST,  8, 3, 1E+9
XPHI    ,  8, 3, 0
XPRICE  ,  4, 1, 5
*XPRTMODE: OPEN - 0x80, PAGING - 0x40, WIDE - 0x20, APPEND - 0x10
XPRTMODE,  4, 6, 0x88840281
XPUNCH  ,  8, 4, 
XRANGE  ,  8, 4, *       
XREAL01 ,  8, 3, 0
XREAL02 ,  8, 3, 0
XREAL03 ,  8, 3, 0
XREAL04 ,  8, 3, 0
XREAL05 ,  8, 3, 0
XREAL06 ,  8, 3, 0
XREAL07 ,  8, 3, 0
XREAL08 ,  8, 3, 0
XREAL09 ,  8, 3, 0
XREAL10 ,  8, 3, 0
XRHS    ,  8, 4, *       
XRMASK  ,  8, 4, 
XRNGFILE,  8, 4, 
XRNGPRNT,  8, 4, NO       
XRSTOP  ,  8, 3, 0
*XRUNMODE: MIP - 0xF7, SLP - 0xF2
XRUNMODE,  4, 6, 0x00
XRYANOSB,  4, 1, 1
XSAVEB  ,  8, 4,         
XSCALESW,  4, 1, 0
XSCRNSW ,  4, 1, 0
XSIF    ,  8, 3, 0
XSINGULR,  4, 5, 13
XSLPNZ  ,  4, 1, 16000
XSOLFILE,  8, 4,         
XSOLPRNT,  8, 4, NO      
XSOLSTAT,  8, 4,         
XSSCALE ,  8, 3, 1
XSSPARSE,  4, 1, 1
XSTEEP  ,  4, 1, 0
XSTEEPCK,  4, 1, 0
XSTEEPNC,  4, 1, 1
XSTEEPRF,  4, 1, 1000
XTABCASE,  8, 4,         
XTHETA  ,  8, 3, 0
XTILEXDF,  4, 1, 0       
XTILWHIZ,  4, 1, 0       
XTIMESAV,  4, 1, 0
XTOLCHK ,  8, 3, 1E-07
XTOLCHZR,  8, 3, 1E-08
XTOLCKRW,  8, 3, 1E-06
XTOLCLRT,  8, 3, 0.001
XTOLDJ  ,  8, 3, 1E-06
XTOLERR ,  8, 3, 1E-05
XTOLFSTM,  8, 3, 1E-09
XTOLINV ,  8, 3, 0.0001
XTOLLMAX,  8, 3, 100.0
XTOLNORM,  8, 3, 0.25
XTOLPERT,  8, 3, 1E-07
XTOLPIV ,  8, 3, 1E-08
XTOLREL ,  8, 3, 1E-10
XTOLRMAX,  8, 3, 1000000
XTOLRWRT,  8, 3, 0.1
XTOLUREL,  8, 3, 1E-09
XTOLV   ,  8, 3, 1E-06
XTOLZE  ,  8, 3, 1E-30
XTRACE  ,  4, 1, 0      
XTRAN   ,  4, 1, 0
XUNRSLV ,  4, 5, 14
XUNBDNDX,  4, 1, 0
XUNIQUES,  4, 1, 100000
XUSEFREE,  4, 1, 0
XZERO   ,  8, 3, 1E-12
SYSUSER , 64, 7, nul
SYSLOG  , 64, 7, matlog.txt
