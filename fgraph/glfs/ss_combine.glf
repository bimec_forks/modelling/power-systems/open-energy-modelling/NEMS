DfFile-RAN1;n:\default\fgraph\RANs\aeo2005.0500z.RAN
DfPgFt-NEMS FGraph - Page @
DfPgTp-5
DfGrTp-10
DfGYLb-Quads
DfGYFm-6
DfGYMn-0
DfGYMx-Data
DfGXLb-Years
DfGXFm-7
DfGXVa-2000 2005 2010 2015 2020 2025 2030
DfSrLn-10 3 1 10 10 10
DfSrCo-1 2 4 5 6 3
PgNext-
PgName-Top 12 Comparison Graphs
PgSubT-(according to OIAF survey)
GrNext-
GrName-Gas Wellhead Price
GrSubT-2003 $/mcf
GrXLab-year
GrYLab-$/mcf
SrNext-
SrName-Place Holder
SrData-RAN1;1;40;2000-2030;SUPA00;ha_GasWellheadPr
GrNext-
GrName-Coal Minemouth Price ($ / ton)
GrSubT-2003 $/ton
GrXLab-year
GrYLab-$/ton
SrNext-
SrName-Place Holder
SrData-RAN1;1;41;2000-2030;SUPA00;ha_CoalMinemouth
GrNext-
GrName-Electricity Price
GrSubT-2003 Cents/kwh
GrXLab-year
GrYLab-Cents/kwh
SrNext-
SrName-Place Holder
SrData-RAN1;1;42;2000-2030;SUPA00;ha_Electricity(c
GrNext-
GrName-New Light-Duty Fuel Economy
GrSubT-miles per gallon
GrXLab-year
GrYLab-mpg
SrNext-
SrName-New Light-Duty Vehicle 2/
SrData-RAN1;7;16;2000-2030;TKIA00;ca_NewLight-Duty
GrNext-
GrName-Electricity Sales
GrSubT-Billion Kilowatthours
GrXLab-year
GrYLab-bkwh
SrNext-
SrName-Total
SrData-RAN1;8;46;2000-2030;ESDA00;ia_Total        
GrNext-
GrName-Greenhouse Gas Intensity Decline from 2002
GrSubT-percent
GrXLab-year
GrYLab-percent
SrNext-
SrName-% Decline from 2002
SrData-RAN1;2;150;2000-2030;QUAA00;na_%Declinefrom2
GrNext-
GrName-Unplanned Additions--Coal
GrSubT-gigawatts
GrXLab-year
GrYLab-GW
SrNext-
SrName-Coal Steam
SrData-RAN1;9;34;2000-2030;EGCA00;ea_CoalSteam    
GrNext-
GrName-Unplanned Additions--Combined Cycle
GrSubT-gigawatts
GrXLab-year
GrYLab-GW
SrNext-
SrName-Combined Cycle
SrData-RAN1;9;36;2000-2030;EGCA00;ea_CombinedCycle
GrNext-
GrName-Unplanned Additions--Combustion Turbine/Diesel
GrSubT-gigawatts
GrXLab-year
GrYLab-GW
SrNext-
SrName-Combustion Turbine/Diesel
SrData-RAN1;9;37;2000-2030;EGCA00;ea_CombustionTur
GrNext-
GrName-Liquefied Natural Gas Imports
GrSubT-thousand cubic feet
GrXLab-year
GrYLab-tcf
SrNext-
SrName-Liquefied Natural Gas 3/
SrData-RAN1;13;9;2000-2030;NGSA00;ca_LiquefiedNatu
GrNext-
GrName-Import Share of Product Supplied
GrSubT-percent
GrXLab-year
GrYLab-percent
SrNext-
SrName-Import Share of Product Supplie
SrData-RAN1;11;44;2000-2030;PSDA00;ha_ImportShareof
GrNext-
GrName-Intercycle Convergence GPA
GrSubT-grade point average
GrXLab-year
GrYLab-GPA
SrNext-
SrName-Intercycle GPA (A:4,B:3,..F:0)
SrData-RAN1;126;51;2000-2030;GPAA00;ga_IntercycleGPA
PgNext-
PgName-Production
PgSubT-(from Table A1)
GrNext-
GrName-Production - Total
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
GrGYMn-Data
SrNext-
SrName-Total
SrData-RAN1;1;10;2000-2030;SUPA00;ba_Total        
GrNext-
GrName-Production - Crude Oil and LC
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Crude Oil & Lease Condensate
SrData-RAN1;1;3;2000-2030;SUPA00;ba_CrudeOilLease
GrNext-
GrName-Production - Natural Gas Plant Liquids
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Natural Gas Plant Liquids
SrData-RAN1;1;4;2000-2030;SUPA00;ba_NaturalGasPla
GrNext-
GrName-Production - Dry Natural Gas
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Dry Natural Gas
SrData-RAN1;1;5;2000-2030;SUPA00;ba_DryNaturalGas
GrNext-
GrName-Production - Coal
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Coal
SrData-RAN1;1;6;2000-2030;SUPA00;ba_Coal         
GrNext-
GrName-Production - Nuclear Power
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Nuclear Power
SrData-RAN1;1;7;2000-2030;SUPA00;ba_NuclearPower 
GrNext-
GrName-Production - Renewable Energy
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Renewable Energy
SrData-RAN1;1;8;2000-2030;SUPA00;ba_RenewableEner
GrNext-
GrName-Production - Other
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Other
SrData-RAN1;1;9;2000-2030;SUPA00;ba_Other        
PgNext-
PgName-Imports, Exports, and Discrepancy
PgSubT-(from Table A1)
GrNext-
GrName-Imports - Total
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Total
SrData-RAN1;1;17;2000-2030;SUPA00;ca_Total        
GrNext-
GrName-Imports - Crude Oil
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Crude Oil
SrData-RAN1;1;13;2000-2030;SUPA00;ca_CrudeOil     
GrNext-
GrName-Imports - Petroleum Products
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Petroleum Products
SrData-RAN1;1;14;2000-2030;SUPA00;ca_PetroleumProd
GrNext-
GrName-Imports - Natural Gas
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Natural Gas
SrData-RAN1;1;15;2000-2030;SUPA00;ca_NaturalGas   
GrNext-
GrName-Imports - Other
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Other Imports
SrData-RAN1;1;16;2000-2030;SUPA00;ca_OtherImports 
GrNext-
GrName-Exports - Total
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Total
SrData-RAN1;1;23;2000-2030;SUPA00;da_Total        
GrNext-
GrName-Exports - Petroleum
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Petroleum
SrData-RAN1;1;20;2000-2030;SUPA00;da_Petroleum    
GrNext-
GrName-Exports - Natural Gas
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Natural Gas
SrData-RAN1;1;21;2000-2030;SUPA00;da_NaturalGas   
GrNext-
GrName-Exports - Coal
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Coal
SrData-RAN1;1;22;2000-2030;SUPA00;da_Coal         
GrNext-
GrName-Discrepancy
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Discrepancy
SrData-RAN1;1;25;2000-2030;SUPA00;ea_Discrepancy  
GrNext-
GrName-Net Imports - Petroleum
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Net Imports - Petroleum
SrData-RAN1;1;36;2000-2030;SUPA00;ga_NetImports-Pe
PgNext-
PgName-Consumption and Prices
PgSubT-(from Table A1)
GrNext-
GrName-Consumption - Total
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Total
SrData-RAN1;1;34;2000-2030;SUPA00;fa_Total        
GrNext-
GrName-Consumption - Petroleum Products
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Petroleum Products
SrData-RAN1;1;28;2000-2030;SUPA00;fa_PetroleumProd
GrNext-
GrName-Consumption - Natural Gas
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Natural Gas
SrData-RAN1;1;29;2000-2030;SUPA00;fa_NaturalGas   
GrNext-
GrName-Consumption - Coal
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Coal
SrData-RAN1;1;30;2000-2030;SUPA00;fa_Coal         
GrNext-
GrName-Consumption - Nuclear Power
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Nuclear Power
SrData-RAN1;1;31;2000-2030;SUPA00;fa_NuclearPower 
GrNext-
GrName-Consumption - Renewable Energy
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Renewable Energy
SrData-RAN1;1;32;2000-2030;SUPA00;fa_RenewableEner
GrNext-
GrName-Consumption - Other
GrSubT-
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Other
SrData-RAN1;1;33;2000-2030;SUPA00;fa_Other        
GrNext-
GrName-Prices - World Oil Price
GrSubT-2003 Dollars per Barrel
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-World Oil Price
SrData-RAN1;1;39;2000-2030;SUPA00;ha_WorldOilPrice
GrNext-
GrName-Prices - Natural Gas Wellhead
GrSubT-2003 Dollars per 1000 Cubic Feet
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Gas Wellhead Price
SrData-RAN1;1;40;2000-2030;SUPA00;ha_GasWellheadPr
GrNext-
GrName-Prices - Coal Minemouth
GrSubT-2003 Dollars per Ton
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Coal Minemouth Price
SrData-RAN1;1;41;2000-2030;SUPA00;ha_CoalMinemouth
GrNext-
GrName-Prices - Average Electricity
GrSubT-2003 Cents per Kilowatthour
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Average Electricity Price
SrData-RAN1;1;42;2000-2030;SUPA00;ha_Electricity(c
PgNext-
PgName-Energy Consumption -- Residential 
PgSubT-(from Table A2)
GrNext-
GrName-Residential Electricity
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Electricity
SrData-RAN1;2;12;2000-2030;QUAA00;ca_Electricity  
GrNext-
GrName-Residential Natural Gas
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Natural Gas
SrData-RAN1;2;9;2000-2030;QUAA00;ca_NaturalGas   
GrNext-
GrName-Residential Distillate Fuel
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Distillate Fuel
SrData-RAN1;2;5;2000-2030;QUAA00;ca_DistillateFue
GrNext-
GrName-Residential Kerosene
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Kerosene
SrData-RAN1;2;6;2000-2030;QUAA00;ca_Kerosene     
GrNext-
GrName-Residential Liquefied Petroleum Gas
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;2;7;2000-2030;QUAA00;ca_LiquefiedPetr
GrNext-
GrName-Residential Petroleum Total
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Petroleum
SrData-RAN1;2;8;2000-2030;QUAA00;ca_PetroleumSubt
GrNext-
GrName-Residential Renewable Energy
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Renewable Energy
SrData-RAN1;2;11;2000-2030;QUAA00;ca_RenewableEner
GrNext-
GrName-Residential Delivered Energy
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Delivered Energy
SrData-RAN1;2;13;2000-2030;QUAA00;ca_DeliveredEner
GrNext-
GrName-Residential Primary Energy
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Total
SrData-RAN1;2;15;2000-2030;QUAA00;ca_Total        
PgNext-
PgName-Energy Consumption -- Commercial
PgSubT-(from Table A2)
GrNext-
GrName-Commercial Electricity
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Electricity
SrData-RAN1;2;27;2000-2030;QUAA00;da_Electricity  
GrNext-
GrName-Commercial Natural Gas
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Natural Gas
SrData-RAN1;2;24;2000-2030;QUAA00;da_NaturalGas   
GrNext-
GrName-Commercial Distillate Fuel
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Distillate Fuel
SrData-RAN1;2;18;2000-2030;QUAA00;da_DistillateFue
GrNext-
GrName-Commercial Residual Fuel
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Residual Fuel
SrData-RAN1;2;19;2000-2030;QUAA00;da_ResidualFuel 
GrNext-
GrName-Commercial Kerosene
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Kerosene
SrData-RAN1;2;20;2000-2030;QUAA00;da_Kerosene     
GrNext-
GrName-Commercial Liquefied Petroleum Gas
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;2;21;2000-2030;QUAA00;da_LiquefiedPetr
GrNext-
GrName-Commercial Motor Gasoline
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Motor Gasoline
SrData-RAN1;2;22;2000-2030;QUAA00;da_MotorGasoline
GrNext-
GrName-Commercial Petroleum Total
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Petroleum Subtotal
SrData-RAN1;2;23;2000-2030;QUAA00;da_PetroleumSubt
GrNext-
GrName-Commercial Coal
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Coal
SrData-RAN1;2;25;2000-2030;QUAA00;da_Coal         
GrNext-
GrName-Commercial Renewable Energy
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Renewable Energy 3/
SrData-RAN1;2;26;2000-2030;QUAA00;da_RenewableEner
GrNext-
GrName-Commercial Delivered Energy
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Delivered Energy
SrData-RAN1;2;28;2000-2030;QUAA00;da_DeliveredEner
GrNext-
GrName-Commercial Primary Energy
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Total
SrData-RAN1;2;30;2000-2030;QUAA00;da_Total        
PgNext-
PgName-Energy Consumption -- Industrial 
PgSubT-(from Table A2)
GrNext-
GrName-Industrial Electricity
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Electricity
SrData-RAN1;2;48;2000-2030;QUAA00;ea_Electricity  
GrNext-
GrName-Industrial Natural Gas
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Natural Gas
SrData-RAN1;2;42;2000-2030;QUAA00;ea_NaturalGasSub
GrNext-
GrName-Industrial Distillate
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Distillate Fuel
SrData-RAN1;2;33;2000-2030;QUAA00;ea_DistillateFue
GrNext-
GrName-Industrial Liquefied Petroleum Gas
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;2;34;2000-2030;QUAA00;ea_LiquefiedPetr
GrNext-
GrName-Industrial Petrochemical Feedstocks
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Petrochemical Feedstocks
SrData-RAN1;2;35;2000-2030;QUAA00;ea_Petrochemical
GrNext-
GrName-Industrial Residual Fuel
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Residual Fuel
SrData-RAN1;2;36;2000-2030;QUAA00;ea_ResidualFuel 
GrNext-
GrName-Industrial Other Petroleum
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Other Petroleum
SrData-RAN1;2;38;2000-2030;QUAA00;ea_OtherPetroleu
GrNext-
GrName-Industrial Metallurgical Coal
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Metallurgical Coal
SrData-RAN1;2;43;2000-2030;QUAA00;ea_Metallurgical
GrNext-
GrName-Industrial Steam Coal
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Steam Coal
SrData-RAN1;2;44;2000-2030;QUAA00;ea_SteamCoal    
GrNext-
GrName-Industrial Net Coal Coke Imports
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Net Coal Coke Imports
SrData-RAN1;2;45;2000-2030;QUAA00;ea_NetCoalCokeIm
GrNext-
GrName-Industrial Renewable Energy
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Renewable Energy
SrData-RAN1;2;47;2000-2030;QUAA00;ea_RenewableEner
GrNext-
GrName-Industrial Primary Energy
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Total
SrData-RAN1;2;51;2000-2030;QUAA00;ea_Total        
PgNext-
PgName-Energy Consumption -- Transportation
PgSubT-(from Table A2)
GrNext-
GrName-Transportation Motor Gasoline
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Motor Gasoline
SrData-RAN1;2;56;2000-2030;QUAA00;fa_MotorGasoline
GrNext-
GrName-Transportation Distillate
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Distillate Fuel
SrData-RAN1;2;54;2000-2030;QUAA00;fa_DistillateFue
GrNext-
GrName-Transportation Jet Fuel
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Jet Fuel
SrData-RAN1;2;55;2000-2030;QUAA00;fa_JetFuel      
GrNext-
GrName-Transportation Residual Fuel
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Residual Fuel
SrData-RAN1;2;57;2000-2030;QUAA00;fa_ResidualFuel 
GrNext-
GrName-Transportation Liquefied Petroleum Gas
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;2;58;2000-2030;QUAA00;fa_LiquefiedPetr
GrNext-
GrName-Transportation Other Petroleum
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Other Petroleum
SrData-RAN1;2;59;2000-2030;QUAA00;fa_OtherPetroleu
GrNext-
GrName-Transportation Total Petroleum
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Petroleum Subtotal
SrData-RAN1;2;60;2000-2030;QUAA00;fa_PetroleumSubt
GrNext-
GrName-Transportation Pipeline Natural Gas
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Pipeline Fuel Natural Gas
SrData-RAN1;2;61;2000-2030;QUAA00;fa_PipelineFuelN
GrNext-
GrName-Transportation CNG
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Compressed Natural Gas
SrData-RAN1;2;62;2000-2030;QUAA00;fa_CompressedNat
GrNext-
GrName-Transportation Electricity
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Electricity
SrData-RAN1;2;65;2000-2030;QUAA00;fa_Electricity  
GrNext-
GrName-Transportation Renewable Energy
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Renewable Energy (E85)
SrData-RAN1;2;63;2000-2030;QUAA00;fa_RenewableEner
GrNext-
GrName-Transportation Primary Energy
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Total
SrData-RAN1;2;68;2000-2030;QUAA00;fa_Total        
PgNext-
PgName-Energy Consumption - Electric Power
PgSubT-(from Table A2)
GrNext-
GrName-Electric Power - Distillate Fuel
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Distillate Fuel
SrData-RAN1;2;96;2000-2030;QUAA00;ha_DistillateFue
GrNext-
GrName-Electric Power - Residual Fuel
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Residual Fuel
SrData-RAN1;2;97;2000-2030;QUAA00;ha_ResidualFuel 
GrNext-
GrName-Electric Power - Petroleum Total
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Petroleum
SrData-RAN1;2;98;2000-2030;QUAA00;ha_PetroleumSubt
GrNext-
GrName-Electric Power - Natural Gas
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Natural Gas
SrData-RAN1;2;99;2000-2030;QUAA00;ha_NaturalGas   
GrNext-
GrName-Electric Power - Steam Coal
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Steam Coal
SrData-RAN1;2;100;2000-2030;QUAA00;ha_SteamCoal    
GrNext-
GrName-Electric Power - Nuclear Power
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Nuclear Power
SrData-RAN1;2;101;2000-2030;QUAA00;ha_NuclearPower 
GrNext-
GrName-Electric Power - Renewable Energy/Other
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Renewable Energy/Other
SrData-RAN1;2;102;2000-2030;QUAA00;ha_RenewableEner
GrNext-
GrName-Electric Power - Imports
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Electricity Imports
SrData-RAN1;2;103;2000-2030;QUAA00;ha_ElectricityIm
GrNext-
GrName-Electric Power - Total
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Total
SrData-RAN1;2;104;2000-2030;QUAA00;ha_Total        
PgNext-
PgName-Energy Consumption - Totals and Indicators
PgSubT-(from Table A2)
GrNext-
GrName-Total Electricity
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Electricity
SrData-RAN1;2;90;2000-2030;QUAA00;ga_Electricity  
GrNext-
GrName-Total Natural Gas
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Natural Gas Subtotal
SrData-RAN1;2;119;2000-2030;QUAA00;ia_NaturalGasSub
GrNext-
GrName-Total Petroleum
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Petroleum Subtotal
SrData-RAN1;2;115;2000-2030;QUAA00;ia_PetroleumSubt
GrNext-
GrName-Total Coal
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Coal Subtotal
SrData-RAN1;2;123;2000-2030;QUAA00;ia_CoalSubtotal 
GrNext-
GrName-Total Renewables
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Renewable Energy
SrData-RAN1;2;125;2000-2030;QUAA00;ia_RenewableEner
GrNext-
GrName-Total Delivered Energy Use
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Delivered Energy Use
SrData-RAN1;2;132;2000-2030;QUAA00;ka_DeliveredEner
GrNext-
GrName-Total Primary Energy Use
GrSubT-Quadrillion Btu
GrXLab-Forecast Year
GrYLab-Quads
SrNext-
SrName-Total Energy Use
SrData-RAN1;2;133;2000-2030;QUAA00;ka_TotalEnergyUs
GrNext-
GrName-Population
GrSubT-Millions
GrXLab-Forecast Year
GrYLab-Millions
SrNext-
SrName-Population (millions)
SrData-RAN1;2;134;2000-2030;QUAA00;ka_Population(mi
GrNext-
GrName-Gross Domestic Product
GrSubT-Billion 2000 Dollars
GrXLab-Forecast Year
GrYLab-96 Dollars
SrNext-
SrName-US GDP (billion 2000 dollars)
SrData-RAN1;2;135;2000-2030;QUAA00;ka_USGDP(billion
GrNext-
GrName-Carbon Dioxide Emissions
GrSubT-Million Metric Tons
GrXLab-Forecast Year
GrYLab-MMT
SrNext-
SrName-Carbon Dioxide
SrData-RAN1;2;137;2000-2030;QUAA00;ka_tonscarbon_dd
GrNext-
GrName-Energy Intensity
GrSubT-Thous. Btu per 2000 Dollars GDP
GrXLab-Forecast Year
GrYLab-mBtu/$
SrNext-
SrName-Energy Intensity
SrData-RAN1;2;145;2000-2030;QUAA00;na_per2000dollar
GrNext-
GrName-Carbon Intensity
GrSubT-Metric Tons CO2 per mill. 2000$ GDP
GrXLab-Forecast Year
GrYLab-mBtu/$
SrNext-
SrName-Carbon Intensity
SrData-RAN1;2;147;2000-2030;QUAA00;na_million2000do
PgNext-
PgName-Energy Prices -- Buildings
PgSubT-From FTab Table A3
GrNext-
GrName-Residential - Electricity
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Electricity
SrData-RAN1;3;8;2000-2030;PRCA00;ba_Electricity  
GrNext-
GrName-Residential - Natural Gas
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Natural Gas
SrData-RAN1;3;7;2000-2030;PRCA00;ba_NaturalGas   
GrNext-
GrName-Residential - Distillate
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Distillate Fuel
SrData-RAN1;3;5;2000-2030;PRCA00;ba_DistillateFue
GrNext-
GrName-Residential - Liquefied Petroleum Gas
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;3;6;2000-2030;PRCA00;ba_LiquefiedPetr
GrNext-
GrName-Commercial - Electricity
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Electricity
SrData-RAN1;3;16;2000-2030;PRCA00;ca_Electricity  
GrNext-
GrName-Commercial - Natural Gas
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Natural Gas
SrData-RAN1;3;15;2000-2030;PRCA00;ca_NaturalGas   
GrNext-
GrName-Commercial - Distillate
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Distillate Fuel
SrData-RAN1;3;13;2000-2030;PRCA00;ca_DistillateFue
GrNext-
GrName-Commercial - Residual Fuel
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Residual Fuel
SrData-RAN1;3;14;2000-2030;PRCA00;ca_ResidualFuel 
PgNext-
PgName-Energy Prices -- Industrial/Transportation
PgSubT-From FTab Table A3
GrNext-
GrName-Industrial - Electricity
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Electricity
SrData-RAN1;3;27;2000-2030;PRCA00;da_Electricity  
GrNext-
GrName-Industrial - Natural Gas
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Natural Gas
SrData-RAN1;3;24;2000-2030;PRCA00;da_NaturalGas   
GrNext-
GrName-Industrial - Distillate
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Distillate Fuel
SrData-RAN1;3;21;2000-2030;PRCA00;da_DistillateFue
GrNext-
GrName-Industrial - Liquefied Petroleum Gas
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;3;22;2000-2030;PRCA00;da_LiquefiedPetr
GrNext-
GrName-Industrial - Residual Fuel
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Residual Fuel
SrData-RAN1;3;23;2000-2030;PRCA00;da_ResidualFuel 
GrNext-
GrName-Industrial - Steam Coal
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Steam Coal
SrData-RAN1;3;26;2000-2030;PRCA00;da_SteamCoal    
GrNext-
GrName-Transportation - Motor Gasoline
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Motor Gasoline
SrData-RAN1;3;34;2000-2030;PRCA00;ea_MotorGasoline
GrNext-
GrName-Transportation - Distillate
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Distillate Fuel
SrData-RAN1;3;32;2000-2030;PRCA00;ea_DistillateFue
GrNext-
GrName-Transportation - Jet Fuel
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Jet Fuel
SrData-RAN1;3;33;2000-2030;PRCA00;ea_JetFuel      
GrNext-
GrName-Transportation - Liquefied Petrolum Gas
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;3;36;2000-2030;PRCA00;ea_LiquefiedPetr
GrNext-
GrName-Transportation - Residual Fuel
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Residual Fuel
SrData-RAN1;3;35;2000-2030;PRCA00;ea_ResidualFuel 
GrNext-
GrName-Transportation - Natural Gas
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Natural Gas
SrData-RAN1;3;37;2000-2030;PRCA00;ea_NaturalGas   
PgNext-
PgName-Energy Prices - Electric Power Sector
PgSubT-From FTab Table A3
GrNext-
GrName-Electric Power - Distillate
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Distillate Fuel
SrData-RAN1;3;48;2000-2030;PRCA00;ga_DistillateFue
GrNext-
GrName-Electric Power - Residual
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Residual Fuel
SrData-RAN1;3;49;2000-2030;PRCA00;ga_ResidualFuel 
GrNext-
GrName-Electric Power - Natural Gas
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Natural Gas
SrData-RAN1;3;50;2000-2030;PRCA00;ga_NaturalGas   
GrNext-
GrName-Electric Power - Steam Coal
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Steam Coal
SrData-RAN1;3;51;2000-2030;PRCA00;ga_SteamCoal    
GrNext-
GrName-Electricity - Average of Sectors
GrSubT-2003 Dollars per Million Btu
GrXLab-Forecast Year
GrYLab-2003 Dollars
SrNext-
SrName-Electricity
SrData-RAN1;3;64;2000-2030;PRCA00;ha_Electricity  
PgNext-
PgName-Industrial Sector -- Shipments, Prices
PgSubT-(from Table 6)
GrNext-
GrName-Value of Shipments - Manufacturing
GrSubT-billion 1996 dollars
GrXLab-Year
GrYLab-Bill. 96$
SrNext-
SrName-Value of Shipments - Manufacturing
SrData-RAN1;6;4;2000-2030;IKIA00;ba_Manufacturing
GrNext-
GrName-Value of Shipments - NonManufacturing
GrSubT-billion 1996 dollars
GrXLab-Year
GrYLab-Bill. 96$
SrNext-
SrName-Nonmanufacturing
SrData-RAN1;6;5;2000-2030;IKIA00;ba_Nonmanufactur
GrNext-
GrName-Value of Shipments - Total
GrSubT-billion 1996 dollars
GrXLab-Year
GrYLab-Bill. 96$
SrNext-
SrName-Total
SrData-RAN1;6;6;2000-2030;IKIA00;ba_Total        
GrNext-
GrName-Energy Prices - Distillate
GrSubT-2003 dollars per million Btu
GrXLab-Year
GrYLab-2003$/mmBtu
SrNext-
SrName-Distillate Oil
SrData-RAN1;6;9;2000-2030;IKIA00;ca_DistillateOil
GrNext-
GrName-Energy Prices - Liquefied Petroleum Gas
GrSubT-2003 dollars per million Btu
GrXLab-Year
GrYLab-2003$/mmBtu
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;6;10;2000-2030;IKIA00;ca_LiquefiedPetr
GrNext-
GrName-Energy Prices - Residual Oil
GrSubT-2003 dollars per million Btu
GrXLab-Year
GrYLab-2003$/mmBtu
SrNext-
SrName-Residual Oil
SrData-RAN1;6;11;2000-2030;IKIA00;ca_ResidualOil  
GrNext-
GrName-Energy Prices - Motor Gasoline
GrSubT-2003 dollars per million Btu
GrXLab-Year
GrYLab-2003$/mmBtu
SrNext-
SrName-Motor Gasoline
SrData-RAN1;6;12;2000-2030;IKIA00;ca_MotorGasoline
GrNext-
GrName-Energy Prices - Natural Gas
GrSubT-2003 dollars per million Btu
GrXLab-Year
GrYLab-2003$/mmBtu
SrNext-
SrName-Natural Gas
SrData-RAN1;6;13;2000-2030;IKIA00;ca_NaturalGas   
GrNext-
GrName-Energy Prices - Metallurgical Coal
GrSubT-2003 dollars per million Btu
GrXLab-Year
GrYLab-2003$/mmBtu
SrNext-
SrName-Metallurgical Coal
SrData-RAN1;6;14;2000-2030;IKIA00;ca_Metallurgical
GrNext-
GrName-Energy Prices - Steam Coal
GrSubT-2003 dollars per million Btu
GrXLab-Year
GrYLab-2003$/mmBtu
SrNext-
SrName-Steam Coal
SrData-RAN1;6;15;2000-2030;IKIA00;ca_SteamCoal    
GrNext-
GrName-Energy Prices - Electricity
GrSubT-2003 dollars per million Btu
GrXLab-Year
GrYLab-2003$/mmBtu
SrNext-
SrName-Electricity
SrData-RAN1;6;16;2000-2030;IKIA00;ca_Electricity  
PgNext-
PgName-Industrial Sector -- Petroleum Consumption
PgSubT-(from Table 6)
GrNext-
GrName-Energy Use - Distillate
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Distillate
SrData-RAN1;6;19;2000-2030;IKIA00;da_Distillate   
GrNext-
GrName-Energy Use - LPG
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;6;20;2000-2030;IKIA00;da_LiquefiedPetr
GrNext-
GrName-Energy Use - Petrochemical Feedstocks
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Petro. Feedstocks
SrData-RAN1;6;21;2000-2030;IKIA00;da_Petrochemical
GrNext-
GrName-Energy Use - Residual Fuel
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Residual Fuel
SrData-RAN1;6;22;2000-2030;IKIA00;da_ResidualFuel 
GrNext-
GrName-Energy Use - Motor Gasoline
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Motor Gasoline
SrData-RAN1;6;23;2000-2030;IKIA00;da_MotorGasoline
GrNext-
GrName-Energy Use - Petroleum Coke
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Petroleum Coke
SrData-RAN1;6;24;2000-2030;IKIA00;da_PetroleumCoke
GrNext-
GrName-Energy Use - Still Gas
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Still Gas
SrData-RAN1;6;25;2000-2030;IKIA00;da_StillGas     
GrNext-
GrName-Energy Use - Asphalt
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Asphalt
SrData-RAN1;6;26;2000-2030;IKIA00;da_Asphalt      
GrNext-
GrName-Energy Use - Miscellaneous Petroleum
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Misc. Petroleum
SrData-RAN1;6;27;2000-2030;IKIA00;da_Miscellaneous
GrNext-
GrName-Energy Use - Petroleum Subtotal
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Petroleum Subtotal
SrData-RAN1;6;28;2000-2030;IKIA00;da_PetroleumSubt
PgNext-
PgName-Industrial Sector -- Other Fuels Consumption
PgSubT-From Table 6
GrNext-
GrName-Energy Use - Natural Gas
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Natural Gas
SrData-RAN1;6;29;2000-2030;IKIA00;da_NaturalGas   
GrNext-
GrName-Energy Use - Lease and Plant Fuel
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Lease and Plant Fuel 3/
SrData-RAN1;6;30;2000-2030;IKIA00;da_LeaseandPlant
GrNext-
GrName-Energy Use - Natural Gas Total
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Natural Gas Subtotal
SrData-RAN1;6;31;2000-2030;IKIA00;da_NaturalGasSub
GrNext-
GrName-Energy Use - Metallurgical Coal & Coke
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Met. Coal & Coke
SrData-RAN1;6;32;2000-2030;IKIA00;da_Metallurgical
GrNext-
GrName-Energy Use - Steam Coal
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Steam Coal
SrData-RAN1;6;33;2000-2030;IKIA00;da_SteamCoal    
GrNext-
GrName-Energy Use - Coal Total
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Coal
SrData-RAN1;6;34;2000-2030;IKIA00;da_CoalSubtotal 
GrNext-
GrName-Energy Use - Renewables
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Renewables 5/
SrData-RAN1;6;35;2000-2030;IKIA00;da_Renewables   
GrNext-
GrName-Energy Use - Purchased Electricity
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Purchased Electricity
SrData-RAN1;6;36;2000-2030;IKIA00;da_PurchasedElec
GrNext-
GrName-Energy Use - Delivered Energy
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Delivered Energy
SrData-RAN1;6;37;2000-2030;IKIA00;da_DeliveredEner
GrNext-
GrName-Energy Use - Electricity Related Losses
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Electricity Related Losses
SrData-RAN1;6;38;2000-2030;IKIA00;da_ElectricityRe
GrNext-
GrName-Energy Use - Primary Energy
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Total
SrData-RAN1;6;39;2000-2030;IKIA00;da_Total        
PgNext-
PgName-Industrial Sector - Petroleum Intensity
PgSubT-From Table 6
GrNext-
GrName-Energy Intensity - Distillate
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Distillate
SrData-RAN1;6;43;2000-2030;IKIA00;ea_Distillate   
GrNext-
GrName-Energy Intensity - Liquefied Petroleum Gas
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;6;44;2000-2030;IKIA00;ea_LiquefiedPetr
GrNext-
GrName-Energy Intensity - Petrochemical Feedstocks
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Petrochemical Feedstocks
SrData-RAN1;6;45;2000-2030;IKIA00;ea_Petrochemical
GrNext-
GrName-Energy Intensity Residual Fuel
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Residual Fuel
SrData-RAN1;6;46;2000-2030;IKIA00;ea_ResidualFuel 
GrNext-
GrName-Energy Intensity - Motor Gasoline
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Motor Gasoline
SrData-RAN1;6;47;2000-2030;IKIA00;ea_MotorGasoline
GrNext-
GrName-Energy Intensity - Petroleum Coke
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Petroleum Coke
SrData-RAN1;6;48;2000-2030;IKIA00;ea_PetroleumCoke
GrNext-
GrName-Energy Intensity - Still Gas
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Still Gas
SrData-RAN1;6;49;2000-2030;IKIA00;ea_StillGas     
GrNext-
GrName-Energy Intensity - Asphalt
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Asphalt
SrData-RAN1;6;50;2000-2030;IKIA00;ea_Asphalt      
GrNext-
GrName-Energy Intensity - Miscellaneous Petroleum
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Miscellaneous Petroleum 2/
SrData-RAN1;6;51;2000-2030;IKIA00;ea_Miscellaneous
GrNext-
GrName-Energy Intensity - Petroleum Total
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Petroleum Subtotal
SrData-RAN1;6;52;2000-2030;IKIA00;ea_PetroleumSubt
PgNext-
PgName-Industrial Sector - Other Fuels Intensity
PgSubT-From Table 6
GrNext-
GrName-Energy Intensity - Natural Gas
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Natural Gas
SrData-RAN1;6;53;2000-2030;IKIA00;ea_NaturalGas   
GrNext-
GrName-Energy Intensity - Lease and Plant Fuel
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Lease and Plant Fuel 3/
SrData-RAN1;6;54;2000-2030;IKIA00;ea_LeaseandPlant
GrNext-
GrName-Energy Intensity - Natural Gas Total
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Natural Gas Subtotal
SrData-RAN1;6;55;2000-2030;IKIA00;ea_NaturalGasSub
GrNext-
GrName-Energy Intensity - Metallurgical Coal & Coke
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Met Coal & Coke
SrData-RAN1;6;56;2000-2030;IKIA00;ea_Metallurgical
GrNext-
GrName-Energy Intensity - Steam Coal
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Steam Coal
SrData-RAN1;6;57;2000-2030;IKIA00;ea_SteamCoal    
GrNext-
GrName-Energy Intensity - Coal Total
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Coal Subtotal
SrData-RAN1;6;58;2000-2030;IKIA00;ea_CoalSubtotal 
GrNext-
GrName-Energy Intensity - Renewables
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Renewables
SrData-RAN1;6;59;2000-2030;IKIA00;ea_Renewables   
GrNext-
GrName-Energy Intensity - Purchased Electricity
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Purchased Electricity
SrData-RAN1;6;60;2000-2030;IKIA00;ea_PurchasedElec
GrNext-
GrName-Energy Intensity - Delivered Energy
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Delivered Energy
SrData-RAN1;6;61;2000-2030;IKIA00;ea_DeliveredEner
GrNext-
GrName-Energy Intensity - Electricity Losses
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Electricity Related Losses
SrData-RAN1;6;62;2000-2030;IKIA00;ea_ElectricityRe
GrNext-
GrName-Energy Intensity - Primary Energy
GrSubT-Thous. Btu per 1996 Dollar of Shipments
GrXLab-Year
GrYLab-mBtu/96$
SrNext-
SrName-Total
SrData-RAN1;6;63;2000-2030;IKIA00;ea_Total        
PgNext-
PgName-Transportation Sector (1 of 3)
PgSubT-(from Table 7)
GrNext-
GrName-Travel - Light-Duty Vehicles 
GrSubT-Billion Vehicle Miles Traveled
GrXLab-Year
GrYLab-Billion Miles
SrNext-
SrName-Light-Duty Vehicles < 8500 lb
SrData-RAN1;7;5;2000-2030;TKIA00;ba_Light-DutyVeh
GrNext-
GrName-Travel - Commercial Light Trucks
GrSubT-Billion Vehicle Miles Traveled
GrXLab-Year
GrYLab-Billion Miles
SrNext-
SrName-Commercial Light Trucks 1/
SrData-RAN1;7;6;2000-2030;TKIA00;ba_CommercialLig
GrNext-
GrName-Travel - Freight Trucks
GrSubT-Billion Vehicle Miles Traveled
GrXLab-Year
GrYLab-Billion Miles
SrNext-
SrName-Freight Trucks > 10000 lbs
SrData-RAN1;7;7;2000-2030;TKIA00;ba_FreightTrucks
GrNext-
GrName-Travel - Air
GrSubT-seat miles available
GrXLab-Year
GrYLab-Billions
SrNext-
SrName-Air
SrData-RAN1;7;9;2000-2030;TKIA00;ba_Air          
GrNext-
GrName-Travel - Rail
GrSubT-Billion Ton-Miles
GrXLab-Year
GrYLab-Billions
SrNext-
SrName-Rail
SrData-RAN1;7;11;2000-2030;TKIA00;ba_Rail         
GrNext-
GrName-Travel - Domestic Shipping
GrSubT-Billion Ton-Miles
GrXLab-Year
GrYLab-Bill Ton-Miles
SrNext-
SrName-Domestic Shipping
SrData-RAN1;7;12;2000-2030;TKIA00;ba_DomesticShipp
GrNext-
GrName-Efficiency - New Light-Duty Vehicles
GrSubT-Miles per Gallon 
GrXLab-Year
GrYLab-MPG
SrNext-
SrName-New Light-Duty Vehicle 2/
SrData-RAN1;7;16;2000-2030;TKIA00;ca_NewLight-Duty
GrNext-
GrName-Efficiency - New Cars
GrSubT-Miles per Gallon 
GrXLab-Year
GrYLab-MPG
SrNext-
SrName-New Car 2/
SrData-RAN1;7;17;2000-2030;TKIA00;ca_NewCar       
GrNext-
GrName-Efficiency - New Light Trucks
GrSubT-Miles per Gallon
GrXLab-Year
GrYLab-MPG
SrNext-
SrName-New Light Truck 2/
SrData-RAN1;7;18;2000-2030;TKIA00;ca_NewLightTruck
GrNext-
GrName-Efficiency - Light-Duty Stock
GrSubT-Miles per Gallon
GrXLab-Year
GrYLab-MPG
SrNext-
SrName-Light-Duty Stock   
SrData-RAN1;7;19;2000-2030;TKIA00;ca_Light-DutySto
GrNext-
GrName-Efficiency - New Commercial Light Trucks
GrSubT-Miles per Gallon
GrXLab-Year
GrYLab-MPG
SrNext-
SrName-New Commercial Light Truck 1/
SrData-RAN1;7;20;2000-2030;TKIA00;ca_NewCommercial
GrNext-
GrName-Efficiency - Stock Comm. Light Trucks
GrSubT-Miles per Gallon
GrXLab-Year
GrYLab-MPG
SrNext-
SrName-Stock Commercial Light Truck
SrData-RAN1;7;21;2000-2030;TKIA00;ca_StockCommerci
PgNext-
PgName-Transportation Sector (2 of 3)
PgSubT-From Table 7
GrNext-
GrName-Efficiency - Aircraft
GrSubT-Seat Miles per Gallon
GrXLab-Year
GrYLab-seat-MPG
SrNext-
SrName-Aircraft
SrData-RAN1;7;24;2000-2030;TKIA00;ca_Aircraft     
GrNext-
GrName-Efficiency - Freight Trucks
GrSubT-Miles per Gallon
GrXLab-Year
GrYLab-MPG
SrNext-
SrName-Freight Truck
SrData-RAN1;7;22;2000-2030;TKIA00;ca_FreightTruck 
GrNext-
GrName-Efficiency - Rail
GrSubT-Ton miles per thousand Btu
GrXLab-Year
GrYLab-ton-miles/mBtu
SrNext-
SrName-Rail
SrData-RAN1;7;26;2000-2030;TKIA00;ca_Rail         
GrNext-
GrName-Energy Use by Mode - Light Duty Vehicles 
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Light-Duty Vehicles
SrData-RAN1;7;31;2000-2030;TKIA00;da_Light-DutyVeh
GrNext-
GrName-Energy Use by Mode - Commercial Light Trucks
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Commercial Light Trucks 1/
SrData-RAN1;7;32;2000-2030;TKIA00;da_CommercialLig
GrNext-
GrName-Energy Use by Mode - Buses
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Bus Transportation
SrData-RAN1;7;33;2000-2030;TKIA00;da_BusTransporta
GrNext-
GrName-Energy Use by Mode - Freight Trucks
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Freight Trucks
SrData-RAN1;7;34;2000-2030;TKIA00;da_FreightTrucks
GrNext-
GrName-Energy Use by Mode - Rail, Passenger
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Rail, Passenger
SrData-RAN1;7;35;2000-2030;TKIA00;da_Rail,Passenge
GrNext-
GrName-Energy Use by Mode - Rail, Freight
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Rail, Freight
SrData-RAN1;7;36;2000-2030;TKIA00;da_Rail,Freight 
GrNext-
GrName-Energy Use by Mode - Shipping, Domestic
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Shipping, Domestic
SrData-RAN1;7;37;2000-2030;TKIA00;da_Shipping,Dome
GrNext-
GrName-Energy Use by Mode - Shipping, International
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Shipping, International
SrData-RAN1;7;38;2000-2030;TKIA00;da_Shipping,Inte
GrNext-
GrName-Energy Use by Mode - Recreational Boats
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Recreational Boats
SrData-RAN1;7;39;2000-2030;TKIA00;da_RecreationalB
PgNext-
PgName-Transportation Sector (3 of 3)
PgSubT-From Table 7
GrNext-
GrName-Energy Use by Mode - Air
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Air
SrData-RAN1;7;40;2000-2030;TKIA00;da_Air          
GrNext-
GrName-Energy Use by Mode - Military
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Military Use
SrData-RAN1;7;41;2000-2030;TKIA00;da_MilitaryUse  
GrNext-
GrName-Energy Use by Mode - Lubricants 
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Lubricants
SrData-RAN1;7;42;2000-2030;TKIA00;da_Lubricants   
GrNext-
GrName-Energy Use by Mode - Pipeline Fuel
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Pipeline Fuel
SrData-RAN1;7;43;2000-2030;TKIA00;da_PipelineFuel 
GrNext-
GrName-Energy Use - All Modes
GrSubT-Quadrillion Btu
GrXLab-Year
GrYLab-Quads
SrNext-
SrName-Total
SrData-RAN1;7;44;2000-2030;TKIA00;da_Total        
PgNext-
PgName-Electricity Generation (1 of 2)
PgSubT-From FTab Table A8
GrNext-
GrName-Power Sector Only Gen. - Coal
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Coal
SrData-RAN1;8;6;2000-2030;ESDA00;ca_Coal         
GrNext-
GrName-Power Sector Only Gen. - Petroleum
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Petroleum
SrData-RAN1;8;7;2000-2030;ESDA00;ca_Petroleum    
GrNext-
GrName-Power Sector Only Gen. - Natural Gas
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Natural Gas
SrData-RAN1;8;8;2000-2030;ESDA00;ca_NaturalGas   
GrNext-
GrName-Power Sector Only Gen. - Nuclear
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Nuclear
SrData-RAN1;8;9;2000-2030;ESDA00;ca_NuclearPower 
GrNext-
GrName-Power Sector Only Gen. - Pumped Storage
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Pumped Storage
SrData-RAN1;8;10;2000-2030;ESDA00;ca_PumpedStorage
GrNext-
GrName-Power Sector Only Gen. - Renewables
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Renewables
SrData-RAN1;8;11;2000-2030;ESDA00;ca_RenewableSour
GrNext-
GrName-Power Sector Only Gen. - DG (Natural Gas)
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-DG (Natural Gas)
SrData-RAN1;8;12;2000-2030;ESDA00;ca_DistributedGe
GrNext-
GrName-Power Sector Only Gen. - Total
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Total
SrData-RAN1;8;13;2000-2030;ESDA00;ca_Total        
GrNext-
GrName-Power Sector CHP Gen. - Coal
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Coal
SrData-RAN1;8;15;2000-2030;ESDA00;da_Coal         
GrNext-
GrName-Power Sector CHP Gen. - Petroleum
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Petroleum
SrData-RAN1;8;16;2000-2030;ESDA00;da_Petroleum    
GrNext-
GrName-Power Sector CHP Gen. - Natural Gas
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Natural Gas
SrData-RAN1;8;17;2000-2030;ESDA00;da_NaturalGas   
PgNext-
PgName-Electricity Generation (1 of 2)
PgSubT-From FTab Table A8
GrNext-
GrName-Power Sector CHP Gen. - Renewables
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Renewable Sources
SrData-RAN1;8;18;2000-2030;ESDA00;da_RenewableSour
GrNext-
GrName-Power Sector CHP Gen. - Total
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Total
SrData-RAN1;8;19;2000-2030;ESDA00;da_Total        
GrNext-
GrName-Power Sector Gen. - Direct Use
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Direct Use
SrData-RAN1;8;21;2000-2030;ESDA00;da_LessDirectUse
GrNext-
GrName-Power Sector Gen. - Net to Grid
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Net Available to the Grid
SrData-RAN1;8;23;2000-2030;ESDA00;ea_NetAvailablet
GrNext-
GrName-End-Use Sector Gener. - Coal
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Coal
SrData-RAN1;8;26;2000-2030;ESDA00;fa_Coal         
GrNext-
GrName-End-Use Sector Gener. - Petroleum
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Petroleum
SrData-RAN1;8;27;2000-2030;ESDA00;fa_Petroleum    
GrNext-
GrName-End-Use Sector Gener. - Natural Gas
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Natural Gas
SrData-RAN1;8;28;2000-2030;ESDA00;fa_NaturalGas   
GrNext-
GrName-End-Use Sector Gener. - Other Gas
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Other Gaseous Fuels 7/
SrData-RAN1;8;29;2000-2030;ESDA00;fa_OtherGaseousF
GrNext-
GrName-End-Use Sector Gener. - Renewables
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Renewable Sources 4/
SrData-RAN1;8;30;2000-2030;ESDA00;fa_RenewableSour
GrNext-
GrName-End-Use Sector Gener. - Other
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Other 8/
SrData-RAN1;8;31;2000-2030;ESDA00;fa_Other        
GrNext-
GrName-End-Use Sector Gener. - Total
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Total
SrData-RAN1;8;32;2000-2030;ESDA00;fa_Total        
PgNext-
PgName-Electricity Disposition, Imports, Sales
PgSubT-From FTab Table A8
GrNext-
GrName-End-Use Sector Gener. - Direct Use
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Direct Use
SrData-RAN1;8;33;2000-2030;ESDA00;fa_LessDirectUse
GrNext-
GrName-End-Use Sector Gener. - Grid Sales
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Total Sales to the Grid
SrData-RAN1;8;34;2000-2030;ESDA00;fa_TotalSalestot
GrNext-
GrName-Total Net Generation to the Grid
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Total Net Generation to the Grid
SrData-RAN1;8;37;2000-2030;ESDA00;ga_TotalNetGener
GrNext-
GrName-Net Imports of Electricity
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Net Imports
SrData-RAN1;8;39;2000-2030;ESDA00;ha_NetImports   
GrNext-
GrName-Residential Electricity Sales
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Residential
SrData-RAN1;8;42;2000-2030;ESDA00;ia_Residential  
GrNext-
GrName-Commercial Electricity Sales
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Commercial
SrData-RAN1;8;43;2000-2030;ESDA00;ia_Commercial   
GrNext-
GrName-Industrial Electricity Sales
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Industrial
SrData-RAN1;8;44;2000-2030;ESDA00;ia_Industrial   
GrNext-
GrName-Transportation Electricity Sales
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Transportation
SrData-RAN1;8;45;2000-2030;ESDA00;ia_Transportatio
GrNext-
GrName-Total Electricity Sales
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Total
SrData-RAN1;8;46;2000-2030;ESDA00;ia_Total        
GrNext-
GrName-Direct Use
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Direct Use
SrData-RAN1;8;47;2000-2030;ESDA00;ia_DirectUse    
GrNext-
GrName-Total Electricity Consumption
GrSubT-Billion Kilowatthours
GrXLab-Forecast Year
GrYLab-BKWH
SrNext-
SrName-Total Consumption
SrData-RAN1;8;48;2000-2030;ESDA00;ia_TotalConsumpt
PgNext-
PgName-Electricity Prices and Emissions
PgSubT-From FTab Table A8
GrNext-
GrName-Residential Electricity Price
GrSubT-2003 Cents per Kilowatthour
GrXLab-Forecast Year
GrYLab-2003 Cents
SrNext-
SrName-Residential
SrData-RAN1;8;51;2000-2030;ESDA00;ja_Residential  
GrNext-
GrName-Commercial Electricity Price
GrSubT-2003 Cents per Kilowatthour
GrXLab-Forecast Year
GrYLab-2003 Cents
SrNext-
SrName-Industrial
SrData-RAN1;8;53;2000-2030;ESDA00;ja_Industrial   
GrNext-
GrName-Industrial Electricity Price
GrSubT-2003 Cents per Kilowatthour
GrXLab-Forecast Year
GrYLab-2003 Cents
SrNext-
SrName-Industrial
SrData-RAN1;8;53;2000-2030;ESDA00;ja_Industrial   
GrNext-
GrName-Transportation Electricity Price
GrSubT-2003 Cents per Kilowatthour
GrXLab-Forecast Year
GrYLab-2003 Cents
SrNext-
SrName-Transportation
SrData-RAN1;8;54;2000-2030;ESDA00;ja_Transportatio
GrNext-
GrName-Overall Average Electricity Price
GrSubT-2003 Cents per Kilowatthour
GrXLab-Forecast Year
GrYLab-2003 Cents
SrNext-
SrName-All Sectors Average
SrData-RAN1;8;55;2000-2030;ESDA00;ja_AllSectorsAve
GrNext-
GrName-Overall Price for Generation
GrSubT-2003 Cents per Kilowatthour
GrXLab-Forecast Year
GrYLab-2003 Cents
SrNext-
SrName-Generation
SrData-RAN1;8;59;2000-2030;ESDA00;ka_Generation   
GrNext-
GrName-Overall Price for Transmission
GrSubT-2003 Cents per Kilowatthour
GrXLab-Forecast Year
GrYLab-2003 Cents
SrNext-
SrName-Transmission
SrData-RAN1;8;60;2000-2030;ESDA00;ka_Transmission 
GrNext-
GrName-Overall Price for Distribution
GrSubT-2003 Cents per Kilowatthour
GrXLab-Forecast Year
GrYLab-2003 Cents
SrNext-
SrName-Distribution
SrData-RAN1;8;61;2000-2030;ESDA00;ka_Distribution 
GrNext-
GrName-Sulfur Dioxide Emissions
GrSubT-Million Tons
GrXLab-Forecast Year
GrYLab-Million Tons
SrNext-
SrName-Sulfur Dioxide (million tons)
SrData-RAN1;8;64;2000-2030;ESDA00;la_SulfurDioxide
GrNext-
GrName-Nitrogen Oxide Emissions
GrSubT-Million Tons
GrXLab-Forecast Year
GrYLab-Million Tons
SrNext-
SrName-Nitrogen Oxide (million tons)
SrData-RAN1;8;65;2000-2030;ESDA00;la_NitrogenOxide
GrNext-
GrName-Mercury Emissions
GrSubT-Tons
GrXLab-Forecast Year
GrYLab-Tons
SrNext-
SrName-Mercury (tons)
SrData-RAN1;8;66;2000-2030;ESDA00;la_Mercury(tons)
PgNext-
PgName-Electricity Generating Capacity (1 of 5)
PgSubT-From FTab Table A9
GrNext-
GrName-Power Sector Only Cap. - Coal Steam
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Coal Steam
SrData-RAN1;9;4;2000-2030;EGCA00;ba_CoalSteam    
GrNext-
GrName-Power Sector Only Cap. - Oth. Foss. Steam
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Other Fossil Steam 4/
SrData-RAN1;9;5;2000-2030;EGCA00;ba_OtherFossilSt
GrNext-
GrName-Power Sector Only Cap. - Combined Cycle
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Combined Cycle
SrData-RAN1;9;6;2000-2030;EGCA00;ba_CombinedCycle
GrNext-
GrName-Power Sector Only Cap. - CT/Diesel
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-CT/Diesel
SrData-RAN1;9;8;2000-2030;EGCA00;ba_NuclearPower 
GrNext-
GrName-Power Sector Only Cap. - Nuclear Power
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Nuclear Power 5/
SrData-RAN1;9;8;2000-2030;EGCA00;ba_NuclearPower 
GrNext-
GrName-Power Sector Only Cap. - Pumped Storage
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Pumped Storage
SrData-RAN1;9;9;2000-2030;EGCA00;ba_PumpedStorage
GrNext-
GrName-Power Sector Only Cap. - Fuel Cells
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Fuel Cells
SrData-RAN1;9;10;2000-2030;EGCA00;ba_FuelCells    
GrNext-
GrName-Power Sector Only Cap. - Renewables
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Renewable Sources 6/
SrData-RAN1;9;11;2000-2030;EGCA00;ba_RenewableSour
GrNext-
GrName-Power Sector Only Cap. - DistGen
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Distributed Generation (Natu
SrData-RAN1;9;12;2000-2030;EGCA00;ba_DistributedGe
GrNext-
GrName-Power Sector Only Cap. - Total
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Total
SrData-RAN1;9;13;2000-2030;EGCA00;ba_Total        
GrNext-
GrName-Power Sector CHP Cap. - Coal Steam
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Coal Steam
SrData-RAN1;9;15;2000-2030;EGCA00;ca_CoalSteam    
GrNext-
GrName-Power Sector CHP Cap. - Other Fossil Steam
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Other Fossil Steam 4/
SrData-RAN1;9;16;2000-2030;EGCA00;ca_OtherFossilSt
PgNext-
PgName-Electricity Generating Capacity (2 of 5)
PgSubT-From FTab Table A9
GrNext-
GrName-Power Sector CHP Cap. - Combined Cycle
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Combined Cycle
SrData-RAN1;9;17;2000-2030;EGCA00;ca_CombinedCycle
GrNext-
GrName-Power Sector CHP Cap. - CT/Diesel
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Combustion Turbine/Diesel
SrData-RAN1;9;18;2000-2030;EGCA00;ca_CombustionTur
GrNext-
GrName-Power Sector CHP Cap. - Renewables
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Renewable Sources 6/
SrData-RAN1;9;19;2000-2030;EGCA00;ca_RenewableSour
GrNext-
GrName-Power Sector CHP Cap. - Total
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Total
SrData-RAN1;9;20;2000-2030;EGCA00;ca_Total        
GrNext-
GrName-Total Electric Power Capacity
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Total Electric Power Sector Capa
SrData-RAN1;9;57;2000-2030;EGCA00;ga_TotalElectric
GrNext-
GrName-Cum. Planned Additions - Coal Steam
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Coal Steam
SrData-RAN1;9;23;2000-2030;EGCA00;da_CoalSteam    
GrNext-
GrName-Cum. Planned Additions - Oth. Foss. Steam
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Other Fossil Steam 4/
SrData-RAN1;9;24;2000-2030;EGCA00;da_OtherFossilSt
GrNext-
GrName-Cum. Planned Additions - Combined Cycle
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Combined Cycle
SrData-RAN1;9;25;2000-2030;EGCA00;da_CombinedCycle
GrNext-
GrName-Cum. Planned Additions - CT/Diesel
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Combustion Turbine/Diesel
SrData-RAN1;9;26;2000-2030;EGCA00;da_CombustionTur
GrNext-
GrName-Cum. Planned Additions - Nuclear Power
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Nuclear Power
SrData-RAN1;9;27;2000-2030;EGCA00;da_NuclearPower 
GrNext-
GrName-Cum. Planned Additions - Pumped Storage
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Pumped Storage
SrData-RAN1;9;28;2000-2030;EGCA00;da_PumpedStorage
GrNext-
GrName-Cum. Planned Additions - Fuel Cells
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Fuel Cells
SrData-RAN1;9;29;2000-2030;EGCA00;da_FuelCells    
PgNext-
PgName-Electricity Generating Capacity (3 of 5)
PgSubT-From FTab Table A9
GrNext-
GrName-Cum. Planned Additions - Renewables
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Renewable Sources 6/
SrData-RAN1;9;30;2000-2030;EGCA00;da_RenewableSour
GrNext-
GrName-Cum. Planned Additions - DistGen
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Distributed Generation 7/
SrData-RAN1;9;31;2000-2030;EGCA00;da_DistributedGe
GrNext-
GrName-Cum. Planned Additions - Total
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Total
SrData-RAN1;9;32;2000-2030;EGCA00;da_Total        
GrNext-
GrName-Cum. Unplanned Additions - Coal Steam
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Coal Steam
SrData-RAN1;9;34;2000-2030;EGCA00;ea_CoalSteam    
GrNext-
GrName-Cum. Unplanned Additions - Oth. Foss. Steam
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Other Fossil Steam 4/
SrData-RAN1;9;35;2000-2030;EGCA00;ea_OtherFossilSt
GrNext-
GrName-Cum. Unplanned Additions - Combined Cycle
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Combined Cycle
SrData-RAN1;9;36;2000-2030;EGCA00;ea_CombinedCycle
GrNext-
GrName-Cum. Unplanned Additions - CT/Diesel
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Combustion Turbine/Diesel
SrData-RAN1;9;37;2000-2030;EGCA00;ea_CombustionTur
GrNext-
GrName-Cum. Unplanned Additions - Nuclear Power
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Nuclear Power
SrData-RAN1;9;38;2000-2030;EGCA00;ea_NuclearPower 
GrNext-
GrName-Cum. Unplanned Additions - Pumped Storage
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Pumped Storage
SrData-RAN1;9;39;2000-2030;EGCA00;ea_PumpedStorage
GrNext-
GrName-Cum. Unplanned Additions - Fuel Cells
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Fuel Cells
SrData-RAN1;9;40;2000-2030;EGCA00;ea_FuelCells    
GrNext-
GrName-Cum. Unplanned Additions - Renewables
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Renewable Sources 6/
SrData-RAN1;9;41;2000-2030;EGCA00;ea_RenewableSour
GrNext-
GrName-Cum. Unplanned Additions - DistGen
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Distributed Generation 7/
SrData-RAN1;9;42;2000-2030;EGCA00;ea_DistributedGe
PgNext-
PgName-Electricity Generating Capacity (4 of 5)
PgSubT-From FTab Table A9
GrNext-
GrName-Cum. Unplanned Additions - Total
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Total
SrData-RAN1;9;43;2000-2030;EGCA00;ea_Total        
GrNext-
GrName-Cumulative Total Additions
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Cumulative Electric Power Sect
SrData-RAN1;9;44;2000-2030;EGCA00;ea_CumulativeEle
GrNext-
GrName-Cumulative Retirements - Coal Steam
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Coal Steam
SrData-RAN1;9;47;2000-2030;EGCA00;fa_CoalSteam    
GrNext-
GrName-Cumulative Retirements - Oth. Foss. Steam
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Other Fossil Steam 4/
SrData-RAN1;9;48;2000-2030;EGCA00;fa_OtherFossilSt
GrNext-
GrName-Cumulative Retirements - Combined Cycle
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Combined Cycle
SrData-RAN1;9;49;2000-2030;EGCA00;fa_CombinedCycle
GrNext-
GrName-Cumulative Retirements - CT/Diesel
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Combustion Turbine/Diesel
SrData-RAN1;9;50;2000-2030;EGCA00;fa_CombustionTur
GrNext-
GrName-Cumulative Retirements - Nuclear Power
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Nuclear Power
SrData-RAN1;9;51;2000-2030;EGCA00;fa_NuclearPower 
GrNext-
GrName-Cumulative Retirements - Pumped Storage
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Pumped Storage
SrData-RAN1;9;52;2000-2030;EGCA00;fa_PumpedStorage
GrNext-
GrName-Cumulative Retirements - Fuel Cells
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Fuel Cells
SrData-RAN1;9;53;2000-2030;EGCA00;fa_FuelCells    
GrNext-
GrName-Cumulative Retirements - Renewables
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Renewable Sources 6/
SrData-RAN1;9;54;2000-2030;EGCA00;fa_RenewableSour
GrNext-
GrName-Cumulative Retirements - Total
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Total
SrData-RAN1;9;55;2000-2030;EGCA00;fa_Total        
GrNext-
GrName-Commercial and Industrial - Coal
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Coal
SrData-RAN1;9;60;2000-2030;EGCA00;ha_Coal         
PgNext-
PgName-Electricity Generating Capacity (5 of 5)
PgSubT-From FTab Table A9
GrNext-
GrName-Commercial and Industrial - Petroleum
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Petroleum
SrData-RAN1;9;61;2000-2030;EGCA00;ha_Petroleum    
GrNext-
GrName-Commercial and Industrial - Natural Gas
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Natural Gas
SrData-RAN1;9;62;2000-2030;EGCA00;ha_NaturalGas   
GrNext-
GrName-Commercial and Industrial - Other Gas
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Other Gaseous Fuels
SrData-RAN1;9;63;2000-2030;EGCA00;ha_OtherGaseousF
GrNext-
GrName-Commercial and Industrial - Renewables
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Renewable Sources 6/
SrData-RAN1;9;64;2000-2030;EGCA00;ha_RenewableSour
GrNext-
GrName-Commercial and Industrial
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Other
SrData-RAN1;9;65;2000-2030;EGCA00;ha_Other        
GrNext-
GrName-Commercial and Industrial - Renewable
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Renewable Sources 6/
SrData-RAN1;9;64;2000-2030;EGCA00;ha_RenewableSour
GrNext-
GrName-Commercial and Industrial - Total
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Total
SrData-RAN1;9;66;2000-2030;EGCA00;ha_Total        
GrNext-
GrName-Commercial and Industrial Additions
GrSubT-Gigawatts
GrXLab-Forecast Year
GrYLab-GW
SrNext-
SrName-Cumulative Capacity Additions
SrData-RAN1;9;68;2000-2030;EGCA00;ia_CumulativeCap
PgNext-
PgName-Petroleum Supply and Disposition (1 of 3)
PgSubT-(from Table 11)
GrNext-
GrName-Supply - Domestic Crude
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Domestic Crude Production
SrData-RAN1;11;3;2000-2030;PSDA00;ba_DomesticCrude
GrNext-
GrName-Crude Oil Supply - Alaska       
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Alaska
SrData-RAN1;11;4;2000-2030;PSDA00;ba_Alaska       
GrNext-
GrName-Crude Oil Supply - Lower 48 States      
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Lower 48 States
SrData-RAN1;11;5;2000-2030;PSDA00;ba_Lower48States
GrNext-
GrName-Crude Oil Supply - Net Imports       
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Net Imports
SrData-RAN1;11;6;2000-2030;PSDA00;ba_NetImports   
GrNext-
GrName-Crude Oil Supply - Gross Imports       
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Gross Imports
SrData-RAN1;11;7;2000-2030;PSDA00;ba_GrossImports 
GrNext-
GrName-Crude Oil Exports        
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Exports
SrData-RAN1;11;8;2000-2030;PSDA00;ba_Exports      
GrNext-
GrName-Crude Oil Supply - Other       
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Other Crude Supply 2/
SrData-RAN1;11;9;2000-2030;PSDA00;ba_OtherCrudeSup
GrNext-
GrName-Crude Oil Supply - Total
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Total Crude Supply
SrData-RAN1;11;10;2000-2030;PSDA00;ba_TotalCrudeSup
GrNext-
GrName-Supply - Natural Gas Plant Liquids
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Natural Gas Plant Liquids
SrData-RAN1;11;13;2000-2030;PSDA00;ca_NaturalGasPla
GrNext-
GrName-Supply - Other Inputs
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Other Inputs 5/
SrData-RAN1;11;20;2000-2030;PSDA00;ca_OtherInputs  
GrNext-
GrName-Supply - Refinery Processing Gain
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Refinery Processing Gain 4/
SrData-RAN1;11;19;2000-2030;PSDA00;ca_RefineryProce
PgNext-
PgName-Petroleum Supply and Disposition (2 of 3)
PgSubT-from Table 11
GrNext-
GrName-Supply - Net Product Imports
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Net Product Imports
SrData-RAN1;11;14;2000-2030;PSDA00;ca_NetProductImp
GrNext-
GrName-Gross Refined Product Imports
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Gross Refined Product Impor
SrData-RAN1;11;15;2000-2030;PSDA00;ca_GrossRefinedP
GrNext-
GrName-Unfinished Oil Imports
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Unfinished Oil Imports
SrData-RAN1;11;16;2000-2030;PSDA00;ca_UnfinishedOil
GrNext-
GrName-Blending Component Imports
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Blending Component Imports
SrData-RAN1;11;17;2000-2030;PSDA00;ca_BlendingCompo
GrNext-
GrName-Refined Product Exports
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Exports
SrData-RAN1;11;18;2000-2030;PSDA00;ca_Exports      
GrNext-
GrName-Total Primary Supply
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Total Primary Supply 6/
SrData-RAN1;11;24;2000-2030;PSDA00;da_TotalPrimaryS
GrNext-
GrName-Refined Products Supplied - Motor Gasoline
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Motor Gasoline 7/
SrData-RAN1;11;27;2000-2030;PSDA00;ea_MotorGasoline
GrNext-
GrName-Refined Products Supplied - Jet Fuel
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Jet Fuel 8/
SrData-RAN1;11;28;2000-2030;PSDA00;ea_JetFuel      
GrNext-
GrName-Refined Products Supplied - Distillate Fuel
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Distillate Fuel 9/
SrData-RAN1;11;29;2000-2030;PSDA00;ea_DistillateFue
GrNext-
GrName-Refined Products Supplied - Residual Fuel
GrSubT-Residual Fuel
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Residual Fuel
SrData-RAN1;11;30;2000-2030;PSDA00;ea_ResidualFuel 
GrNext-
GrName-Refined Products Supplied - Other
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Other 10/
SrData-RAN1;11;31;2000-2030;PSDA00;ea_Other        
GrNext-
GrName-Refined Products Supplied - Total
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Total
SrData-RAN1;11;32;2000-2030;PSDA00;ea_Total        
PgNext-
PgName-Petroleum Supply and Disposition Balance (3 of 3)
PgSubT-(from Table 11)
GrNext-
GrName-Refined Products Supplied - Residential and Commercial
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Residential and Commercial
SrData-RAN1;11;35;2000-2030;PSDA00;fa_Residentialan
GrNext-
GrName-Refined Products Supplied - Industrial
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Industrial 11/
SrData-RAN1;11;36;2000-2030;PSDA00;fa_Industrial   
GrNext-
GrName-Refined Products Supplied - Transportation
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Electric Power 12/
SrData-RAN1;11;38;2000-2030;PSDA00;fa_ElectricPower
GrNext-
GrName-Refined Products Supplied - Electric Power
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Electric Power 12/
SrData-RAN1;11;38;2000-2030;PSDA00;fa_ElectricPower
GrNext-
GrName-Refined Products Supplied - Total
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Total
SrData-RAN1;11;39;2000-2030;PSDA00;fa_Total        
GrNext-
GrName-Supply-Disposition Discrepancy
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Discrepancy 13/
SrData-RAN1;11;41;2000-2030;PSDA00;ga_Discrepancy  
GrNext-
GrName-World Oil Price
GrSubT-2003 Dollars per Barrel
GrXLab-Year
GrYLab-2003 $/bbl
SrNext-
SrName-World Oil Price (2003 dollars p
SrData-RAN1;11;43;2000-2030;PSDA00;ha_WorldOilPrice
GrNext-
GrName-Import Share of Product Supplied
GrSubT-Fraction
GrXLab-Year
GrYLab-fraction
SrNext-
SrName-Import Share of Product Supplie
SrData-RAN1;11;44;2000-2030;PSDA00;ha_ImportShareof
GrNext-
GrName-Net Expenditures for Product Imports
GrSubT-Billions 2003 Dollars
GrXLab-Year
GrYLab-Bill. 2003$
SrNext-
SrName-Petroleum Products (billion 2
SrData-RAN1;11;46;2000-2030;PSDA00;ha_PetroleumProd
GrNext-
GrName-Domestic Refinery Distillation
GrSubT-Million Barrels/Day
GrXLab-Year
GrYLab-mmBBL/Day
SrNext-
SrName-Domestic Refinery Distillation
SrData-RAN1;11;47;2000-2030;PSDA00;ha_DomesticRefin
GrNext-
GrName-Capacity Utilization Rate
GrSubT-Percent
GrXLab-Year
GrYLab-Percent
SrNext-
SrName-Capacity Utilization Rate
SrData-RAN1;11;48;2000-2030;PSDA00;ha_CapacityUtili
Remark----------------------------------------------------------------- Page
PgNext-
PgName-Petroleum Product Prices (1 of 2)
PgSubT-(from Table 12)
GrNext-
GrName-World Oil Price
GrSubT-2003 dollars per barrel
GrXLab-Year
GrYLab-2003 $/bbl
SrNext-
SrName-World Oil Price (2003 dollars p
SrData-RAN1;12;2;2000-2030;PPPA00;ba_WorldOilPrice
GrNext-
GrName-Residential - Distillate Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Distillate Fuel
SrData-RAN1;12;7;2000-2030;PPPA00;da_DistillateFue
GrNext-
GrName-Residential - LPG
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;12;8;2000-2030;PPPA00;da_LiquefiedPetr
GrNext-
GrName-Commercial - Distillate Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Distillate Fuel
SrData-RAN1;12;11;2000-2030;PPPA00;ea_DistillateFue
GrNext-
GrName-Commercial - Residual Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Residual Fuel
SrData-RAN1;12;12;2000-2030;PPPA00;ea_ResidualFuel 
GrNext-
GrName-Industrial - Distillate Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Distillate Fuel
SrData-RAN1;12;16;2000-2030;PPPA00;fa_DistillateFue
GrNext-
GrName-Industrial - LPG
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;12;17;2000-2030;PPPA00;fa_LiquefiedPetr
GrNext-
GrName-Industrial - Residual Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Residual Fuel
SrData-RAN1;12;18;2000-2030;PPPA00;fa_ResidualFuel 
GrNext-
GrName-Transportation - Diesel Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Diesel Fuel (Distillate) 2/
SrData-RAN1;12;22;2000-2030;PPPA00;ga_DieselFuel(Di
GrNext-
GrName-Transportation - Jet Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Jet Fuel 3/
SrData-RAN1;12;23;2000-2030;PPPA00;ga_JetFuel      
GrNext-
GrName-Transportation - Motor Gasoline
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Motor Gasoline 4/
SrData-RAN1;12;24;2000-2030;PPPA00;ga_MotorGasoline
GrNext-
GrName-Transportation - LPG
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;12;25;2000-2030;PPPA00;ga_LiquefiedPetr
Remark------------------------------------------------------------- Page
PgNext-
PgName-Petroleum Product Prices (2 of 2)
PgSubT-(from Table 12)
GrNext-
GrName-Transportation - Residual Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Residual Fuel
SrData-RAN1;12;26;2000-2030;PPPA00;ga_ResidualFuel 
GrNext-
GrName-Transportation - Ethanol (E85)
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Ethanol (E85)
SrData-RAN1;12;28;2000-2030;PPPA00;ga_Ethanol(E85) 
GrNext-
GrName-Electric Generators - Distillate Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Distillate Fuel
SrData-RAN1;12;31;2000-2030;PPPA00;ha_DistillateFue
GrNext-
GrName-Electric Generators - Residual Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Residual Fuel
SrData-RAN1;12;32;2000-2030;PPPA00;ha_ResidualFuel 
GrNext-
GrName-Refined Petroleum - Distillate Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Distillate Fuel
SrData-RAN1;12;36;2000-2030;PPPA00;ia_DistillateFue
GrNext-
GrName-Refined Petroleum - Jet Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Jet Fuel 3/
SrData-RAN1;12;37;2000-2030;PPPA00;ia_JetFuel      
GrNext-
GrName-Refined Petroleum - LPG
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Liquefied Petroleum Gas
SrData-RAN1;12;38;2000-2030;PPPA00;ia_LiquefiedPetr
GrNext-
GrName-Refined Petroleum - Motor Gasoline
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Motor Gasoline 4/
SrData-RAN1;12;39;2000-2030;PPPA00;ia_MotorGasoline
GrNext-
GrName-Refined Petroleum - Residual Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Residual Fuel
SrData-RAN1;12;40;2000-2030;PPPA00;ia_ResidualFuel 
GrNext-
GrName-Refined Petroleum - Average Fuel
GrSubT-2003 cents/gallon
GrXLab-Year
GrYLab-2003 C/gal
SrNext-
SrName-Average
SrData-RAN1;12;42;2000-2030;PPPA00;ia_Average      
PgNext-
PgName-Natural Gas Supply, Disposition, and Prices (1 of 3)
PgSubT-(from Table 13)
GrNext-
GrName-Production - Dry Gas Production
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Dry Gas Production 1/
SrData-RAN1;13;3;2000-2030;NGSA00;ba_DryGasProduct
GrNext-
GrName-Production - Supplemental Natural Gas
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Supplemental Natural Gas 2/
SrData-RAN1;13;4;2000-2030;NGSA00;ba_SupplementalN
GrNext-
GrName-Net Imports - Canada
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Canada
SrData-RAN1;13;7;2000-2030;NGSA00;ca_Canada       
GrNext-
GrName-Net Imports - Mexico
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Mexico
SrData-RAN1;13;8;2000-2030;NGSA00;ca_Mexico       
GrNext-
GrName-Net Imports - LNG
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Liquefied Natural Gas
SrData-RAN1;13;9;2000-2030;NGSA00;ca_LiquefiedNatu
GrNext-
GrName-Net Imports - Total
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Net Imports - Total
SrData-RAN1;13;6;2000-2030;NGSA00;ca_NetImports   
GrNext-
GrName-Total Supply
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Total Supply
SrData-RAN1;13;11;2000-2030;NGSA00;da_TotalSupply  
GrNext-
GrName-Consumption - Residential
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Residential
SrData-RAN1;13;14;2000-2030;NGSA00;ea_Residential  
GrNext-
GrName-Consumption - Commercial
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Commercial
SrData-RAN1;13;15;2000-2030;NGSA00;ea_Commercial   
GrNext-
GrName-Consumption - Industrial
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Industrial 3/
SrData-RAN1;13;16;2000-2030;NGSA00;ea_Industrial   
GrNext-
GrName-Consumption - Electricity Power
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Electric Generators 4/
SrData-RAN1;13;17;2000-2030;NGSA00;ea_ElectricPower
GrNext-
GrName-Consumption - Transportation Sector
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Transportation 5/
SrData-RAN1;13;18;2000-2030;NGSA00;ea_Transportatio
PgNext-
PgName-Natural Gas Supply, Disposition, and Prices (2 of 3)
PgSubT-
GrNext-
GrName-Consumption - Pipeline Fuel
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Pipeline Fuel
SrData-RAN1;13;19;2000-2030;NGSA00;ea_PipelineFuel 
GrNext-
GrName-Consumption - Lease and Plant Fuel
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Lease and Plant Fuel 6/
SrData-RAN1;13;20;2000-2030;NGSA00;ea_LeaseandPlant
GrNext-
GrName-Consumption - Total
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Total
SrData-RAN1;13;21;2000-2030;NGSA00;ea_Total        
GrNext-
GrName-Consumption - Gas to Liquids
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Gas to Liquids
SrData-RAN1;13;23;2000-2030;NGSA00;fa_GastoLiquids 
GrNext-
GrName-Supply-Disposition Discrepancy
GrSubT-Trillion Cubic Feet
GrXLab-Year
GrYLab-TCF
SrNext-
SrName-Discrepancy 7/
SrData-RAN1;13;25;2000-2030;NGSA00;ga_Discrepancy  
GrNext-
GrName-Lower 48 Wellhead Price
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Average Lower 48 Wellhead Price
SrData-RAN1;13;30;2000-2030;NGSA00;ia_AverageLower4
GrNext-
GrName-Natural Gas Price - Residential
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Residential
SrData-RAN1;13;33;2000-2030;NGSA00;ja_Residential  
GrNext-
GrName-Natural Gas Price - Commercial
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Commercial
SrData-RAN1;13;34;2000-2030;NGSA00;ja_Commercial   
GrNext-
GrName-Natural Gas Price - Industrial
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Industrial 3/
SrData-RAN1;13;35;2000-2030;NGSA00;ja_Industrial   
GrNext-
GrName-Natural Gas Price - Electric Power
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Electric Power 4/
SrData-RAN1;13;36;2000-2030;NGSA00;ja_ElectricPower
GrNext-
GrName-Natural Gas Price - Transportation
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Transportation 9/
SrData-RAN1;13;37;2000-2030;NGSA00;ja_Transportatio
GrNext-
GrName-Natural Gas Price - Average Delivered
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Average 10/
SrData-RAN1;13;38;2000-2030;NGSA00;ja_Average      
PgNext-
PgName-Natural Gas Supply, Disposition, and Prices (3 of 3)
PgSubT-From Table 13
GrNext-
GrName-Average Import Price
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Average Import Price
SrData-RAN1;13;43;2000-2030;NGSA00;ka_AverageImport
GrNext-
GrName-Average Source Price
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Average Source Price
SrData-RAN1;13;44;2000-2030;NGSA00;ka_AverageSource
GrNext-
GrName-Trans. & Dist. Markup - Residential
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Residential
SrData-RAN1;13;47;2000-2030;NGSA00;la_Residential  
GrNext-
GrName-Trans. & Dist. Markup - Commercial
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Commercial
SrData-RAN1;13;48;2000-2030;NGSA00;la_Commercial   
GrNext-
GrName-Trans. & Dist. Markup - Industrial
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Industrial
SrData-RAN1;13;49;2000-2030;NGSA00;la_Industrial   
GrNext-
GrName-Trans. & Dist. Markup - Electric Power
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Electric Power
SrData-RAN1;13;50;2000-2030;NGSA00;la_ElectricPower
GrNext-
GrName-Trans. & Dist. Markup - Transportation
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Transportation
SrData-RAN1;13;51;2000-2030;NGSA00;la_Transportatio
GrNext-
GrName-Trans. & Dist. Markup - Average
GrSubT-2003 Dollars per mcf
GrXLab-Year
GrYLab-2003$/mcf
SrNext-
SrName-Average
SrData-RAN1;13;52;2000-2030;NGSA00;la_Average      
PgNext-
PgName-Domestic Oil and Gas Supply (1 of 2)
PgSubT-(from Table 14)
GrNext-
GrName-Crude Oil - Lower 48 Average Price
GrSubT-2003 dollars/barrel
GrXLab-Year
GrYLab-2003 $/bbl
SrNext-
SrName-(2003 dollars per barrel)
SrData-RAN1;14;5;2000-2030;OGSA00;ca_(2003dollarsp
GrNext-
GrName-Crude Production - U.S. Total
GrSubT-Million barrels/day
GrXLab-Year
GrYLab-MMBBL/DAY
SrNext-
SrName-U.S. Total
SrData-RAN1;14;8;2000-2030;OGSA00;da_UnitedStatesT
GrNext-
GrName-Crude Production - Lower 48 Onshore
GrSubT-Million barrels/day
GrXLab-Year
GrYLab-MMBBL/DAY
SrNext-
SrName-Lower 48 Onshore
SrData-RAN1;14;9;2000-2030;OGSA00;da_Lower48Onshor
GrNext-
GrName-Crude Production - Lower 48 Offshore
GrSubT-Million barrels/day
GrXLab-Year
GrYLab-MMBBL/DAY
SrNext-
SrName-Lower 48 Offshore
SrData-RAN1;14;10;2000-2030;OGSA00;da_Lower48Offsho
GrNext-
GrName-Crude Production - Alaska
GrSubT-Million barrels/day
GrXLab-Year
GrYLab-MMBBL/DAY
SrNext-
SrName-Alaska
SrData-RAN1;14;11;2000-2030;OGSA00;da_Alaska       
GrNext-
GrName-L48 End of Year Reserves
GrSubT-Billion Barrels
GrXLab-Year
GrYLab-Bill. BBLs
SrNext-
SrName-L48 End of Year Reserves (billio
SrData-RAN1;14;13;2000-2030;OGSA00;ea_L48EndofYearR
GrNext-
GrName-Natural Gas - Lower 48 Average
GrSubT-2003 dollars per thousand cubic feet
GrXLab-Year
GrYLab-2003 $/mcf
SrNext-
SrName-(2003 dollars per thousand cubic
SrData-RAN1;14;18;2000-2030;OGSA00;ga_(2003dollarsp
GrNext-
GrName-Dry Natural Gas Production - U.S. Total
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-U.S. Total
SrData-RAN1;14;21;2000-2030;OGSA00;ha_UnitedStatesT
GrNext-
GrName-Dry Production - Lower 48 Onshore
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Lower 48 Onshore
SrData-RAN1;14;22;2000-2030;OGSA00;ha_Lower48Onshor
GrNext-
GrName-Dry Production - Associated-Dissolved
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Associated-Dissolved 4/
SrData-RAN1;14;23;2000-2030;OGSA00;ha_Associated-Di
GrNext-
GrName-Dry Production - Non-Associated
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Non-Associated
SrData-RAN1;14;24;2000-2030;OGSA00;ha_Non-Associate
GrNext-
GrName-Dry Production - Conventional
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Conventional
SrData-RAN1;14;25;2000-2030;OGSA00;ha_Conventional 
PgNext-
PgName-Domestic Oil and Gas Supply (2 of 2)
PgSubT-(from Table 14)
GrNext-
GrName-Dry Production - Unconventional
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Unconventional
SrData-RAN1;14;26;2000-2030;OGSA00;ha_Unconventiona
GrNext-
GrName-Dry Production - Lower 48 Offshore
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Lower 48 Offshore
SrData-RAN1;14;27;2000-2030;OGSA00;ha_Lower48Offsho
GrNext-
GrName-Dry Production - Associated-Dissolved
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Associated-Dissolved 4/
SrData-RAN1;14;28;2000-2030;OGSA00;ia_Associated-Di
GrNext-
GrName-Dry Production - Non-Associated
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Non-Associated
SrData-RAN1;14;29;2000-2030;OGSA00;ia_Non-Associate
GrNext-
GrName-Dry Production - Alaska
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Alaska
SrData-RAN1;14;30;2000-2030;OGSA00;ia_Alaska       
GrNext-
GrName-Lower 48 End of Year Dry Reserve
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Lower 48 End of Year Dry Reserve
SrData-RAN1;14;32;2000-2030;OGSA00;ja_Lower48EndofY
GrNext-
GrName-Supplemental Gas Supplies
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Supplemental Gas Supplies (tcf)5
SrData-RAN1;14;34;2000-2030;OGSA00;ka_SupplementalG
GrNext-
GrName-Lower 48 Wells 
GrSubT-Thousands
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Lower 48 Wells (thousands)
SrData-RAN1;14;36;2000-2030;OGSA00;la_TotalLower48W
GrNext-
GrName-Cumulative Production - Crude Oil
GrSubT-Billion barrels
GrXLab-Year
GrYLab-Bill. BBLs
SrNext-
SrName-Crude Oil (billion barrels)
SrData-RAN1;14;48;2000-2030;OGSA00;oa_CrudeOil(bill
GrNext-
GrName-Cumulative Produce - Lower 48 Dry Gas
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Lower 48 Dry Gas
SrData-RAN1;14;50;2000-2030;OGSA00;oa_Lower48DryGas
GrNext-
GrName-Natural Gas - Lower 48 Non-Associated Gas
GrSubT-Trillion cubic feet
GrXLab-Year
GrYLab-tcf
SrNext-
SrName-Lower 48 Non-Associated Gas
SrData-RAN1;14;51;2000-2030;OGSA00;oa_Lower48Non-As
PgNext-
PgName-Coal Supply, Disposition, and Prices (1 of 2)
PgSubT-(from Table 15)
GrNext-
GrName-Production - Appalachia
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Appalachia
SrData-RAN1;15;3;2000-2030;CSDA00;ba_Appalachia   
GrNext-
GrName-Production - Interior
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Interior
SrData-RAN1;15;4;2000-2030;CSDA00;ba_Interior     
GrNext-
GrName-Production - West
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-West
SrData-RAN1;15;5;2000-2030;CSDA00;ba_West         
GrNext-
GrName-Production - East of Mississippi
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-East of the Mississippi
SrData-RAN1;15;7;2000-2030;CSDA00;ca_EastoftheMiss
GrNext-
GrName-Production - West of Mississippi
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-West of the Mississippi
SrData-RAN1;15;8;2000-2030;CSDA00;ca_WestoftheMiss
GrNext-
GrName-Production - Total
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Total
SrData-RAN1;15;9;2000-2030;CSDA00;ca_Total        
GrNext-
GrName-Imports
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Imports
SrData-RAN1;15;12;2000-2030;CSDA00;da_Imports      
GrNext-
GrName-Exports
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Coal Exports
SrData-RAN1;15;13;2000-2030;CSDA00;da_Exports      
GrNext-
GrName-Net Imports 
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Total
SrData-RAN1;15;14;2000-2030;CSDA00;da_Total        
GrNext-
GrName-Total Supply
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Total Supply 2/
SrData-RAN1;15;16;2000-2030;CSDA00;ea_TotalSupply  
GrNext-
GrName-Consumption - Residential and Commercial
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Residential and Commercial
SrData-RAN1;15;19;2000-2030;CSDA00;fa_Residentialan
GrNext-
GrName-Consumption - Industrial
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Industrial 3/
SrData-RAN1;15;20;2000-2030;CSDA00;fa_Industrial   
PgNext-
PgName-Coal Supply, Disposition, and Prices (2 of 2)
PgSubT-(from Table 15)
GrNext-
GrName-Consumption - Coke Plants
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Coke Plants
SrData-RAN1;15;21;2000-2030;CSDA00;fa_CokePlants   
GrNext-
GrName-Consumption - Electricity Sector
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Electric Power 4/
SrData-RAN1;15;22;2000-2030;CSDA00;fa_ElectricPower
GrNext-
GrName-Consumption - Total
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Total Sectoral Consumption
SrData-RAN1;15;23;2000-2030;CSDA00;fa_TotalSectoral
GrNext-
GrName-Coal to Liquids - Total
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Total Coal Use
SrData-RAN1;15;27;2000-2030;CSDA00;fa_TotalCoalUse 
GrNext-
GrName-Discrepancy and Stock Change
GrSubT-Million Short Tons
GrXLab-Year
GrYLab-MM Sh.Tons
SrNext-
SrName-Discrepancy and Stock Change 5/
SrData-RAN1;15;29;2000-2030;CSDA00;ga_Discrepancyan
GrNext-
GrName-Average Minemouth Price
GrSubT-2003 dollars/short ton
GrXLab-Year
GrYLab-2003 $/ton
SrNext-
SrName-(2003 dollars per short ton)
SrData-RAN1;15;32;2000-2030;CSDA00;ha_(2003dollarsp
GrNext-
GrName-Delivered Price - Industrial
GrSubT-2003 dollars/short ton
GrXLab-Year
GrYLab-2003 $/ton
SrNext-
SrName-Industrial
SrData-RAN1;15;36;2000-2030;CSDA00;ia_Industrial   
GrNext-
GrName-Delivered Price - Coke Plants
GrSubT-2003 dollars/short ton
GrXLab-Year
GrYLab-2003 $/ton
SrNext-
SrName-Coke Plants
SrData-RAN1;15;37;2000-2030;CSDA00;ia_CokePlants   
GrNext-
GrName-Delivered Price - Electricity Power
GrSubT-2003 dollars/short ton
GrXLab-Year
GrYLab-2003 $/ton
SrNext-
SrName-(2003 dollars / short ton)
SrData-RAN1;15;39;2000-2030;CSDA00;ia_(2003dollars/
GrNext-
GrName-Delivered Price - Coal to Liquids
GrSubT-2003 dollars/short ton
GrXLab-Year
GrYLab-2003 $/ton
SrNext-
SrName-Coal to Liquids
SrData-RAN1;15;41;2000-2030;CSDA00;ja_CoaltoLiquids
GrNext-
GrName-Delivered Price - Average
GrSubT-2003 dollars/short ton
GrXLab-Year
GrYLab-2003 $/ton
SrNext-
SrName-Average
SrData-RAN1;15;42;2000-2030;CSDA00;ja_Average      
GrNext-
GrName-Delivered Price - Exports
GrSubT-2003 dollars/short ton
GrXLab-Year
GrYLab-2003 $/ton
SrNext-
SrName-Exports 7/
SrData-RAN1;15;43;2000-2030;CSDA00;ja_Exports      
PgNext-
PgName-Renewable Energy Capacity and Generation (1 of 3)
PgSubT-(from Table 16)
GrNext-
GrName-Electricity Sector - Net Summer Capacity
GrSubT-Conventional Hydropower (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Conventional Hydropower
SrData-RAN1;16;4;2000-2030;RENA00;ba_ConventionalH
GrNext-
GrName-Electricity Sector - Net Summer Capacity
GrSubT-Geothermal (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Geothermal 2/
SrData-RAN1;16;5;2000-2030;RENA00;ba_Geothermal   
GrNext-
GrName-Electricity Sector - Net Summer Capacity
GrSubT-Municipal Solid Waste (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Municipal Solid Waste 3/
SrData-RAN1;16;6;2000-2030;RENA00;ba_MunicipalSoli
GrNext-
GrName-Electricity Sector - Net Summer Capacity
GrSubT-Wood and Other Biomass (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Wood and Other Biomass 4/
SrData-RAN1;16;7;2000-2030;RENA00;ba_WoodandOtherB
GrNext-
GrName-Electricity Sector - Net Summer Capacity
GrSubT-Solar Thermal (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Solar Thermal
SrData-RAN1;16;8;2000-2030;RENA00;ba_SolarThermal 
GrNext-
GrName-Electricity Sector - Net Summer Capacity
GrSubT-Solar Photvoltaic (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Solar Photovoltaic 5/
SrData-RAN1;16;9;2000-2030;RENA00;ba_SolarPhotovol
GrNext-
GrName-Electricity Sector - Net Summer Capacity
GrSubT-Wind (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Wind
SrData-RAN1;16;10;2000-2030;RENA00;ba_Wind         
GrNext-
GrName-Electricity Sector - Net Summer Capacity
GrSubT-Total (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Total
SrData-RAN1;16;11;2000-2030;RENA00;ba_Total        
GrNext-
GrName-Electricity Sector - Generation 
GrSubT-Conventional Hydropower (Billion Kilowatthours)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Conventional Hydropower
SrData-RAN1;16;14;2000-2030;RENA00;ca_ConventionalH
GrNext-
GrName-Electricity Sector - Generation 
GrSubT-Geothermal (Billion Kilowatthours)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Geothermal 2/
SrData-RAN1;16;15;2000-2030;RENA00;ca_Geothermal   
GrNext-
GrName-Electricity Sector - Generation 
GrSubT-Municipal Solid Waste (Billion Kilowatthours)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Municipal Solid Waste 3/
SrData-RAN1;16;16;2000-2030;RENA00;ca_MunicipalSoli
GrNext-
GrName-Electricity Sector - Generation 
GrSubT-Wood and Other Biomass (Billion Kilowatthours)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Wood and Other Biomass 4/
SrData-RAN1;16;17;2000-2030;RENA00;ca_WoodandOtherB
PgNext-
PgName-Renewable Energy Capacity and Generation (2 of 3)
PgSubT-(from Table 16)
GrNext-
GrName-Electricity Sector - Generation 
GrSubT-Wood and Other Biomass - Dedicated Plants (BKWH)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Dedicated Plants
SrData-RAN1;16;18;2000-2030;RENA00;ca_DedicatedPlan
GrNext-
GrName-Electricity Sector - Generation 
GrSubT-Wood and Other Biomass - Cofiring (BKWH)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Cofiring
SrData-RAN1;16;19;2000-2030;RENA00;ca_Cofiring     
GrNext-
GrName-Electricity Sector - Generation 
GrSubT-Solar Thermal (Billion Kilowatthours)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Solar Thermal
SrData-RAN1;16;20;2000-2030;RENA00;ca_SolarThermal 
GrNext-
GrName-Electricity Sector - Generation 
GrSubT-Solar Photovoltaic (Billion Kilowatthours)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Solar Photovoltaic 5/
SrData-RAN1;16;21;2000-2030;RENA00;ca_SolarPhotovol
GrNext-
GrName-Electricity Sector - Generation 
GrSubT-Wind (Billion Kilowatthours)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Wind
SrData-RAN1;16;22;2000-2030;RENA00;ca_Wind         
GrNext-
GrName-Electricity Sector - Generation 
GrSubT-Total (Billion Kilowatthours)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Total
SrData-RAN1;16;23;2000-2030;RENA00;ca_Total        
GrNext-
GrName-End Use Sector Capacity
GrSubT-Conventional Hydropower (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Conventional Hydropower 9/
SrData-RAN1;16;27;2000-2030;RENA00;da_ConventionalH
GrNext-
GrName-End Use Sector Capacity 
GrSubT-Geothermal (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Geothermal
SrData-RAN1;16;28;2000-2030;RENA00;da_Geothermal   
GrNext-
GrName-End Use Sector Capacity
GrSubT-Municipal Solid Waste (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Municipal Solid Waste
SrData-RAN1;16;29;2000-2030;RENA00;da_MunicipalSoli
GrNext-
GrName-End Use Sector Capacity
GrSubT-Biomass (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Biomass
SrData-RAN1;16;30;2000-2030;RENA00;da_Biomass      
GrNext-
GrName-End Use Sector Capacity
GrSubT-Solar Photovoltaic (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Solar Photovoltaic 6/
SrData-RAN1;16;31;2000-2030;RENA00;da_SolarPhotovol
GrNext-
GrName-End Use Sector Capacity
GrSubT-Total (Gigawatts)
GrXLab-Year
GrYLab-Gigawatts
SrNext-
SrName-Total
SrData-RAN1;16;32;2000-2030;RENA00;da_Total        
PgNext-
PgName-Renewable Energy Generating Capacity (3 of 3)
PgSubT-(from Table 16)
GrNext-
GrName-End Use Sector Generation
GrSubT-Conventional Hydropower (BKWH)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Conventional Hydropower 9/
SrData-RAN1;16;35;2000-2030;RENA00;ea_ConventionalH
GrNext-
GrName-End Use Sector Generation
GrSubT-Geothermal (BKWH)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Geothermal
SrData-RAN1;16;36;2000-2030;RENA00;ea_Geothermal   
GrNext-
GrName-End Use Sector Generation
GrSubT-Municipal Solid Waste (BKWH)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Municipal Solid Waste
SrData-RAN1;16;37;2000-2030;RENA00;ea_MunicipalSoli
GrNext-
GrName-End Use Sector Generation
GrSubT-Biomass(BKWH)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Biomass
SrData-RAN1;16;38;2000-2030;RENA00;ea_Biomass      
GrNext-
GrName-End Use Sector Generation
GrSubT-Solar Photovoltaic (Gigawatts)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Solar Photovoltaic 6/
SrData-RAN1;16;39;2000-2030;RENA00;ea_SolarPhotovol
GrNext-
GrName-End Use Sector Generation
GrSubT-Total (BKWH)
GrXLab-Year
GrYLab-BKWH
SrNext-
SrName-Total
SrData-RAN1;16;40;2000-2030;RENA00;ea_Total        
PgNext-
PgName-Carbon Dioxide Emissions -- Residential/Commercial
PgSubT-(from Table 17)
GrNext-
GrName-Residential - Petroleum
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Petroleum
SrData-RAN1;17;3;2000-2030;TCEA00;ba_Petroleum    
GrNext-
GrName-Residential - Natural Gas
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Natural Gas
SrData-RAN1;17;4;2000-2030;TCEA00;ba_NaturalGas   
GrNext-
GrName-Residential - Coal
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Coal
SrData-RAN1;17;5;2000-2030;TCEA00;ba_Coal         
GrNext-
GrName-Residential - Electricity
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Electricity
SrData-RAN1;17;6;2000-2030;TCEA00;ba_Electricity  
GrNext-
GrName-Residential - Total
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Total
SrData-RAN1;17;7;2000-2030;TCEA00;ba_Total        
GrNext-
GrName-Commercial - Petroleum
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Petroleum
SrData-RAN1;17;10;2000-2030;TCEA00;ca_Petroleum    
GrNext-
GrName-Commercial - Natural Gas
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Natural Gas
SrData-RAN1;17;11;2000-2030;TCEA00;ca_NaturalGas   
GrNext-
GrName-Commercial - Coal
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Coal
SrData-RAN1;17;12;2000-2030;TCEA00;ca_Coal         
GrNext-
GrName-Commercial - Electricity
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Electricity
SrData-RAN1;17;13;2000-2030;TCEA00;ca_Electricity  
GrNext-
GrName-Commercial - Total
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Total
SrData-RAN1;17;14;2000-2030;TCEA00;ca_Total        
PgNext-
PgName-Carbon Dioxide Emissions--Industrial/Transportation
PgSubT-(from Table 17)
GrNext-
GrName-Industrial - Petroleum
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Petroleum
SrData-RAN1;17;17;2000-2030;TCEA00;da_Petroleum    
GrNext-
GrName-Industrial - Natural Gas
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Natural Gas 2/
SrData-RAN1;17;18;2000-2030;TCEA00;da_NaturalGas   
GrNext-
GrName-Industrial - Coal
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Coal
SrData-RAN1;17;19;2000-2030;TCEA00;da_Coal         
GrNext-
GrName-Industrial - Electricity
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Electricity
SrData-RAN1;17;20;2000-2030;TCEA00;da_Electricity  
GrNext-
GrName-Industrial - Total
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Total
SrData-RAN1;17;21;2000-2030;TCEA00;da_Total        
GrNext-
GrName-Transportation - Petroleum
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Petroleum 3/
SrData-RAN1;17;24;2000-2030;TCEA00;ea_Petroleum    
GrNext-
GrName-Transportation - Natural Gas
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Natural Gas 4/
SrData-RAN1;17;25;2000-2030;TCEA00;ea_NaturalGas   
GrNext-
GrName-Transportation - Electricity
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Electricity
SrData-RAN1;17;26;2000-2030;TCEA00;ea_Electricity  
GrNext-
GrName-Transportation - Total
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Total
SrData-RAN1;17;27;2000-2030;TCEA00;ea_Total        
PgNext-
PgName-Carbon Dioxide Emissions--Electricity/Total
PgSubT-(from Table 17)
GrNext-
GrName-Electric Power Sector - Petroleum
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Petroleum
SrData-RAN1;17;30;2000-2030;TCEA00;fa_Petroleum    
GrNext-
GrName-Electric Power Sector - Natural Gas
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Natural Gas
SrData-RAN1;17;31;2000-2030;TCEA00;fa_NaturalGas   
GrNext-
GrName-Electric Power Sector - Coal
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Coal
SrData-RAN1;17;32;2000-2030;TCEA00;fa_Coal         
GrNext-
GrName-Electric Power Sector - Other
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Other 6/
SrData-RAN1;17;33;2000-2030;TCEA00;fa_Other        
GrNext-
GrName-Electric Power Sector - Total
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Total
SrData-RAN1;17;34;2000-2030;TCEA00;fa_Total        
GrNext-
GrName-Total by Primary Fuel - Petroleum
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Petroleum 3/
SrData-RAN1;17;37;2000-2030;TCEA00;ga_Petroleum    
GrNext-
GrName-Total by Primary Fuel - Natural Gas
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Natural Gas
SrData-RAN1;17;38;2000-2030;TCEA00;ga_NaturalGas   
GrNext-
GrName-Total by Primary Fuel - Coal
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Coal
SrData-RAN1;17;39;2000-2030;TCEA00;ga_Coal         
GrNext-
GrName-Total by Primary Fuel - Other
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Other 5/
SrData-RAN1;17;40;2000-2030;TCEA00;ga_Other        
GrNext-
GrName-Total by Primary Fuel - Total
GrSubT-million metric tons carbon dioxide equivalent
GrXLab-Year
GrYLab-Mill Met. Tons
SrNext-
SrName-Total
SrData-RAN1;17;41;2000-2030;TCEA00;ga_Total        
GrNext-
GrName-Carbon Dioxide Emisions per capita
GrSubT-tons carbon dioxide equivalent/person
GrXLab-Year
GrYLab-Tons CO2
SrNext-
SrName-equivalent per person)
SrData-RAN1;17;44;2000-2030;TCEA00;ha_equivalentper
PgNext-
PgName-Macroeconomic Indicators - Absolute Levels (1 of 3) 
PgSubT-(from Table 18)
GrNext-
GrName-Real Gross Domestic Product
GrSubT-Billion 2000 Chain-Weighted Dollars
GrXLab-Year
GrYLab-Bill. 2000$
SrNext-
SrName-Real Gross Domestic Product
SrData-RAN1;18;2;2000-2030;MEIA00;ba_RealGrossDome
GrNext-
GrName-Potential Gross Domestic Product
GrSubT-Billion 2000 Chain-Weighted Dollars
GrXLab-Year
GrYLab-Bill. 2000$
SrNext-
SrName-Potential Gross Domestic Product
SrData-RAN1;18;3;2000-2030;MEIA00;ba_RealPotential
GrNext-
GrName-Real Disposable Personal Income
GrSubT-Billion 2000 Chain-Weighted Dollars
GrXLab-Year
GrYLab-Bill. 2000$ 
SrNext-
SrName-Real Disposable Personal Income
SrData-RAN1;18;4;2000-2030;MEIA00;ba_RealDisposabl
GrNext-
GrName-GDP Components - Real Consumption
GrSubT-Billion 2000 Chain-Weighted Dollars
GrXLab-Year
GrYLab-Bill. 2000$
SrNext-
SrName-Real Consumption
SrData-RAN1;18;6;2000-2030;MEIA00;ba_RealConsumpti
GrNext-
GrName-GDP Components - Real Investment
GrSubT-Billion 2000 Chain-Weighted Dollars
GrXLab-Year
GrYLab-Bill. 2000$
SrNext-
SrName-Real Investment
SrData-RAN1;18;7;2000-2030;MEIA00;ba_RealInvestmen
GrNext-
GrName-GDP Components - Real Government Spending
GrSubT-Billion 2000 Chain-Weighted Dollars
GrXLab-Year
GrYLab-Bill. 2000$
SrNext-
SrName-Real Government Spending
SrData-RAN1;18;8;2000-2030;MEIA00;ba_RealGovernmen
GrNext-
GrName-GDP Components - Real Exports
GrSubT-Billion 2000 Chain-Weighted Dollars
GrXLab-Year
GrYLab-Bill. 2000$
SrNext-
SrName-Real Exports
SrData-RAN1;18;9;2000-2030;MEIA00;ba_RealExports  
GrNext-
GrName-GDP Components - Real Imports
GrSubT-Billion 2000 Chain-Weighted Dollars
GrXLab-Year
GrYLab-Bill. 2000$
SrNext-
SrName-Real Imports
SrData-RAN1;18;10;2000-2030;MEIA00;ba_RealImports  
GrNext-
GrName-Energy Intensity - Delivered Energy
GrSubT-Thousand Btu/2000 dollar of GDP
GrXLab-Year
GrYLab-mBtu/2000$GDP
SrNext-
SrName-Delivered Energy
SrData-RAN1;18;14;2000-2030;MEIA00;ca_DeliveredEner
GrNext-
GrName-Energy Intensity - Total Energy
GrSubT-Thousand Btu/2000 dollar of GDP
GrXLab-Year
GrYLab-mBtu/2000$GD
SrNext-
SrName-Total Energy
SrData-RAN1;18;15;2000-2030;MEIA00;ca_TotalEnergy  
PgNext-
PgName-Macroeconomic Indicators - Absolute Levels (2 of 3)
PgSubT-(from Table 18)
GrNext-
GrName-GDP Chain-Type Price Index
GrSubT-2000=1.000
GrXLab-Year
GrYLab-2000=1.000
SrNext-
SrName-(2000=1.000)
SrData-RAN1;18;18;2000-2030;MEIA00;da_GDPChain-Type
GrNext-
GrName-Consumer Price Index
GrSubT-1982-4=1.0
GrXLab-Year
GrYLab-1982-4=1.0
SrNext-
SrName-Consumer Price Index (1982-4=1
SrData-RAN1;18;19;2000-2030;MEIA00;da_ConsumerPrice
GrNext-
GrName-Wholesale Price Index--All 
GrSubT-1982-4=1.0
GrXLab-Year
GrYLab-1982-4=1.0
SrNext-
SrName-All Commodities
SrData-RAN1;18;21;2000-2030;MEIA00;da_AllCommoditie
GrNext-
GrName-Wholesale Price Index--Fuel & Power
GrSubT-1982-4=1.0
GrXLab-Year
GrYLab-1982-4=1.0
SrNext-
SrName-Fuel and Power
SrData-RAN1;18;22;2000-2030;MEIA00;da_FuelandPower 
GrNext-
GrName-Federal Funds Rate
GrSubT-Nominal Percent
GrXLab-Year
GrYLab-Percent
SrNext-
SrName-Federal Funds Rate
SrData-RAN1;18;25;2000-2030;MEIA00;ea_FederalFundsR
GrNext-
GrName-10-Year Treasury Note
GrSubT-Nominal Percent
GrXLab-Year
GrYLab-Percent
SrNext-
SrName-10-Year Treasury Note
SrData-RAN1;18;26;2000-2030;MEIA00;ea_10-YearTreasu
GrNext-
GrName-AA Utility Bond Rate 
GrSubT-Nominal Percent
GrXLab-Year
GrYLab-Percent
SrNext-
SrName-AA Utility Bond Rate
SrData-RAN1;18;27;2000-2030;MEIA00;ea_AAUtilityBond
GrNext-
GrName-Unemployment Rate
GrSubT-percent
GrXLab-Year
GrYLab-percent
SrNext-
SrName-Unemployment Rate (percent)
SrData-RAN1;18;29;2000-2030;MEIA00;fa_UnemploymentR
GrNext-
GrName-Housing Starts
GrSubT-Millions
GrXLab-Year
GrYLab-Millions
SrNext-
SrName-Housing Starts (millions)
SrData-RAN1;18;31;2000-2030;MEIA00;ga_HousingStarts
GrNext-
GrName-Commercial Floorspace, Total
GrSubT-Billion Square Feet
GrXLab-Year
GrYLab-Bill.Sq.Ft.
SrNext-
SrName-(billion square feet)
SrData-RAN1;18;34;2000-2030;MEIA00;ha_(billionsquar
GrNext-
GrName-Unit Sales of Light-Duty Vehicles
GrSubT-millions
GrXLab-Year
GrYLab-millions
SrNext-
SrName-(millions)
SrData-RAN1;18;36;2000-2030;MEIA00;ia_UnitSalesofLi
PgNext-
PgName-Macroeconomic Indicators - Absolute Levels (3 of 3)
PgSubT-(from Table 18)
GrNext-
GrName-Value of Shipments - Total Industrial
GrSubT-Billion 2000 dollars
GrXLab-Year
GrYLab-Bill.2000$
SrNext-
SrName-Total Industrial
SrData-RAN1;18;39;2000-2030;MEIA00;ja_TotalIndustri
GrNext-
GrName-Shipments - Non-Manufacturing
GrSubT-Billion 2000 dollars
GrXLab-Year
GrYLab-Bill.2000$
SrNext-
SrName-Non-Manufacturing
SrData-RAN1;18;40;2000-2030;MEIA00;ja_Non-Manufactu
GrNext-
GrName-Shipments - Manufacturing
GrSubT-Billion 2000 dollars
GrXLab-Year
GrYLab-Bill.2000$
SrNext-
SrName-Manufacturing
SrData-RAN1;18;41;2000-2030;MEIA00;ja_Manufacturing
GrNext-
GrName-Shipments - Energy Intensive Manu.
GrSubT-Billion 2000 dollars
GrXLab-Year
GrYLab-Bill.2000$
SrNext-
SrName-Energy Intensive
SrData-RAN1;18;42;2000-2030;MEIA00;ja_EnergyIntensi
GrNext-
GrName-Shipments - Non-Intensive Manu.
GrSubT-Billion 2000 dollars
GrXLab-Year
GrYLab-Bill.2000$
SrNext-
SrName-Non-Intensive
SrData-RAN1;18;43;2000-2030;MEIA00;ja_Non-EnergyInt
GrNext-
GrName-Population with Armed Forces Overseas
GrSubT-Millions
GrXLab-Year
GrYLab-Million 
SrNext-
SrName-Population w/Armed Forces Over
SrData-RAN1;18;46;2000-2030;MEIA00;ka_Populationwit
GrNext-
GrName-Population Aged 16 and Older
GrSubT-Millions
GrXLab-Year
GrYLab-Million people
SrNext-
SrName-Population (aged 16 and over)
SrData-RAN1;18;47;2000-2030;MEIA00;ka_Populationage
GrNext-
GrName-Employment, Non-Agriculture
GrSubT-Millions
GrXLab-Year
GrYLab-Million
SrNext-
SrName-Employment, Non-Agriculture
SrData-RAN1;18;48;2000-2030;MEIA00;ka_Employment,No
GrNext-
GrName-Employment, Manufacturing
GrSubT-Millions
GrXLab-Year
GrYLab-Million
SrNext-
SrName-Employment, Manufacturing
SrData-RAN1;18;49;2000-2030;MEIA00;ka_Employment,Ma
GrNext-
GrName-Labor Force
GrSubT-Millions
GrXLab-Year
GrYLab-Million
SrNext-
SrName-Labor Force
SrData-RAN1;18;50;2000-2030;MEIA00;ka_LaborForce   
PgNext-
PgName-Macroeconomic Indicators - Differences (1 of 3) 
PgSubT-(from Table 18)
GrNext-
GrName-Real Gross Domestic Product
GrSubT-Billion 2000 Chain-Weighted Dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Real Gross Domestic Product
SrData-RAN1;18;2;2000-2030;MEIA00;ba_RealGrossDome
GrNext-
GrName-Potential Gross Domestic Product
GrSubT-Billion 2000 Dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Real Potential Gross Domestic Pr
SrData-RAN1;18;3;2000-2030;MEIA00;ba_RealPotential
GrNext-
GrName-Real Disposable Personal Income
GrSubT-Billion 2000 Chain-Weighted Dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Real Disposable Personal Income
SrData-RAN1;18;4;2000-2030;MEIA00;ba_RealDisposabl
GrNext-
GrName-GDP Components - Real Consumption
GrSubT-Billion 2000 Chain-Weighted Dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Real Consumption
SrData-RAN1;18;6;2000-2030;MEIA00;ba_RealConsumpti
GrNext-
GrName-GDP Components - Real Investment
GrSubT-Billion 2000 Chain-Weighted Dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Real Investment
SrData-RAN1;18;7;2000-2030;MEIA00;ba_RealInvestmen
GrNext-
GrName-GDP Components - Real Government Spending
GrSubT-Billion 2000 Chain-Weighted Dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Real Government Spending
SrData-RAN1;18;8;2000-2030;MEIA00;ba_RealGovernmen
GrNext-
GrName-GDP Components - Real Exports
GrSubT-Billion 2000 Chain-Weighted Dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Real Exports
SrData-RAN1;18;9;2000-2030;MEIA00;ba_RealExports  
GrNext-
GrName-GDP Components - Real Imports
GrSubT-Billion 2000 Chain-Weighted Dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Real Imports
SrData-RAN1;18;10;2000-2030;MEIA00;ba_RealImports  
GrNext-
GrName-Energy Intensity - Delivered Energy
GrSubT-Thousand Btu/2000 dollar of GDP -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Delivered Energy
SrData-RAN1;18;14;2000-2030;MEIA00;ca_DeliveredEner
GrNext-
GrName-Energy Intensity - Total Energy
GrSubT-Thousand Btu/2000 dollar of GDP -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Total Energy
SrData-RAN1;18;15;2000-2030;MEIA00;ca_TotalEnergy  
PgNext-
PgName-Macroeconomic Indicators - Differences (2 of 3)
PgSubT-(from Table 18)
GrNext-
GrName-GDP Chain-Type Price Index
GrSubT-2000=1.000 -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-(2000=1.000)
SrData-RAN1;18;18;2000-2030;MEIA00;da_GDPChain-Type
GrNext-
GrName-Consumer Price Index
GrSubT-1982-4=1.0 -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Consumer Price Index (1982-4=1
SrData-RAN1;18;19;2000-2030;MEIA00;da_ConsumerPrice
GrNext-
GrName-Wholesale Price Index--All
GrSubT-1982-4=1.0 -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-All Commodities
SrData-RAN1;18;21;2000-2030;MEIA00;da_AllCommoditie
GrNext-
GrName-Wholesale Price Index--Fuel and Power
GrSubT-1982-4=1.0 -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Fuel and Power
SrData-RAN1;18;22;2000-2030;MEIA00;da_FuelandPower 
GrNext-
GrName-Federal Funds Rate
GrSubT-Nominal Percent -- Difference from Base
GrXLab-Year
GrYLab-Percent
GrVals-1
SrNext-
SrName-Federal Funds Rate
SrData-RAN1;18;25;2000-2030;MEIA00;ea_FederalFundsR
GrNext-
GrName-10-Year Treasury Note 
GrSubT-Nominal Percent -- Difference from Base
GrXLab-Year
GrYLab-Percent
GrVals-1
SrNext-
SrName-10-Year Treasury Note
SrData-RAN1;18;26;2000-2030;MEIA00;ea_10-YearTreasu
GrNext-
GrName-AA Utility Bond Rate - Nominal 
GrSubT-Nominal Percent -- Difference from Base
GrXLab-Year
GrYLab-Percent
GrVals-1
SrNext-
SrName-AA Utility Bond Rate
SrData-RAN1;18;27;2000-2030;MEIA00;ea_AAUtilityBond
GrNext-
GrName-Unemployment Rate
GrSubT-percent -- Difference from Base
GrXLab-Year
GrYLab-percent
GrVals-1
SrNext-
SrName-Unemployment Rate (percent)
SrData-RAN1;18;29;2000-2030;MEIA00;fa_UnemploymentR
GrNext-
GrName-Housing Starts
GrSubT-Millions -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Housing Starts (millions)
SrData-RAN1;18;31;2000-2030;MEIA00;ga_HousingStarts
GrNext-
GrName-Commercial Floorspace, Total
GrSubT-Billion Square Feet -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-(billion square feet)
SrData-RAN1;18;34;2000-2030;MEIA00;ha_(billionsquar
GrNext-
GrName-Unit Sales of Light-Duty Vehicles
GrSubT-millions -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-(millions)
SrData-RAN1;18;36;2000-2030;MEIA00;ia_UnitSalesofLi
PgNext-
PgName-Macroeconomic Indicators - Differences (3 of 3)
PgSubT-(from Table 18)
GrNext-
GrName-Value of Shipments - Total Industrial
GrSubT-Billion 2000 dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Total Industrial
SrData-RAN1;18;39;2000-2030;MEIA00;ja_TotalIndustri
GrNext-
GrName-Shipments - Non-Manufacturing
GrSubT-Billion 2000 dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Non-Manufacturing
SrData-RAN1;18;40;2000-2030;MEIA00;ja_Non-Manufactu
GrNext-
GrName-Shipments - Manufacturing
GrSubT-Billion 2000 dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Manufacturing
SrData-RAN1;18;41;2000-2030;MEIA00;ja_Manufacturing
GrNext-
GrName-Shipments - Energy Intensive Manu.
GrSubT-Billion 2000 dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Energy Intensive
SrData-RAN1;18;42;2000-2030;MEIA00;ja_EnergyIntensi
GrNext-
GrName-Shipments - Non-Intensive Manu.
GrSubT-Billion 2000 dollars -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Non-Intensive
SrData-RAN1;18;43;2000-2030;MEIA00;ja_Non-EnergyInt
GrNext-
GrName-Population with Armed Forces Overseas
GrSubT-Millions -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Population w/Armed Forces Over
SrData-RAN1;18;46;2000-2030;MEIA00;ka_Populationwit
GrNext-
GrName-Population Aged 16 and Older
GrSubT-Millions -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Population (aged 16 and over)
SrData-RAN1;18;47;2000-2030;MEIA00;ka_Populationage
GrNext-
GrName-Employment, Non-Agriculture
GrSubT-Millions -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Employment, Non-Agriculture
SrData-RAN1;18;48;2000-2030;MEIA00;ka_Employment,No
GrNext-
GrName-Employment, Manufacturing
GrSubT-Millions -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Employment, Manufacturing
SrData-RAN1;18;49;2000-2030;MEIA00;ka_Employment,Ma
GrNext-
GrName-Labor Force
GrSubT-Millions -- % Change from Base
GrXLab-Year
GrYLab-% Change
GrVals-2
SrNext-
SrName-Labor Force
SrData-RAN1;18;50;2000-2030;MEIA00;ka_LaborForce   
PgNext-
PgName-International Petroleum Supply and Disposition (1 of 5)
PgSubT-(from Table 19)
GrNext-
GrName-World Oil Price
GrSubT-2003 Dollars per Barrel
GrXLab-Year
GrYLab-2003$/BBL
SrNext-
SrName-World Oil Price (2003 $ / barrel
SrData-RAN1;19;2;2000-2030;IPSA00;ba_WorldOilPrice
GrNext-
GrName-Conventional Production, Industrialized Countries
GrSubT-U.S. (50 States)
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-U.S. (50 states)
SrData-RAN1;19;6;2000-2030;IPSA00;ca_U.S.(50states
GrNext-
GrName-Conventional Production, Industrialized Countries
GrSubT-Canada
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Canada
SrData-RAN1;19;7;2000-2030;IPSA00;ca_Canada       
GrNext-
GrName-Conventional Production, Industrialized Countries
GrSubT-Mexico
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Mexico
SrData-RAN1;19;8;2000-2030;IPSA00;ca_Mexico       
GrNext-
GrName-Conventional Production, Industrialized Countries
GrSubT-Western Europe
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Western Europe 3/
SrData-RAN1;19;9;2000-2030;IPSA00;ca_WesternEurope
GrNext-
GrName-Conventional Production, Industrialized Countries
GrSubT-Japan
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Japan
SrData-RAN1;19;10;2000-2030;IPSA00;ca_Japan        
GrNext-
GrName-Conventional Production, Industrialized Countries
GrSubT-Australia and New Zealand
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Australia and New Zealand
SrData-RAN1;19;11;2000-2030;IPSA00;ca_AustraliaandN
GrNext-
GrName-Production, Industrial Countries 
GrSubT-Total
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Total Industrialized
SrData-RAN1;19;12;2000-2030;IPSA00;ca_TotalIndustri
GrNext-
GrName-Conventional Production, Eurasia/FSU
GrSubT-Russia
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Russia
SrData-RAN1;19;16;2000-2030;IPSA00;da_Russia       
GrNext-
GrName-Conventional Production, Eurasia/FSU
GrSubT-Caspian Area
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Caspian Area 4/
SrData-RAN1;19;17;2000-2030;IPSA00;da_CaspianArea  
GrNext-
GrName-Conventional Production, Eurasia/FSU
GrSubT-Eastern Europe
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Eastern Europe 5/
SrData-RAN1;19;18;2000-2030;IPSA00;da_EasternEurope
GrNext-
GrName-Conventional Production, Eurasia/FSU
GrSubT-Total
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Total Eurasia
SrData-RAN1;19;19;2000-2030;IPSA00;da_TotalEurasia 
PgNext-
PgName-International Petroleum Supply and Disposition (2 of 5)
PgSubT-From Table 19
GrNext-
GrName-Conventional Production, Developing Countries
GrSubT-OPEC - Asia
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Asia
SrData-RAN1;19;23;2000-2030;IPSA00;ea_Asia         
GrNext-
GrName-Conventional Production, Developing Countries
GrSubT-OPEC - Middle East
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Middle East
SrData-RAN1;19;24;2000-2030;IPSA00;ea_MiddleEast   
GrNext-
GrName-Conventional Production, Developing Countries
GrSubT-OPEC - North Africa
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-North Africa
SrData-RAN1;19;25;2000-2030;IPSA00;ea_NorthAfrica  
GrNext-
GrName-Conventional Production, Developing Countries
GrSubT-OPEC - West Africa
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-West Africa
SrData-RAN1;19;26;2000-2030;IPSA00;ea_WestAfrica   
GrNext-
GrName-Conventional Production, Developing Countries
GrSubT-OPEC - South America
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-South America
SrData-RAN1;19;27;2000-2030;IPSA00;ea_SouthAmerica 
GrNext-
GrName-Conventional Production, Developing Countries
GrSubT-Non-OPEC, China
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-China
SrData-RAN1;19;29;2000-2030;IPSA00;ea_China        
GrNext-
GrName-Conventional Production, Developing Countries
GrSubT-Non-OPEC, Other Asia
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Other Asia
SrData-RAN1;19;30;2000-2030;IPSA00;ea_OtherAsia    
GrNext-
GrName-Conventional Production, Developing Countries
GrSubT-Non-OPEC, Middle East
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Middle East 7/
SrData-RAN1;19;31;2000-2030;IPSA00;fa_MiddleEast   
GrNext-
GrName-Conventional Production, Developing Countries
GrSubT-Non-OPEC, Africa
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Africa
SrData-RAN1;19;32;2000-2030;IPSA00;fa_Africa       
GrNext-
GrName-Conventional Production, Developing Countries
GrSubT-Non-OPEC, South and Central America
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-South and Central America
SrData-RAN1;19;33;2000-2030;IPSA00;fa_SouthandCentr
GrNext-
GrName-Conventional Production, Developing Countries 
GrSubT-Total OPEC and Non-OPEC
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Total Developing Countri
SrData-RAN1;19;34;2000-2030;IPSA00;fa_TotalDevelopi
GrNext-
GrName-Conventional Production, Total 
GrSubT-Million barrels/day
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Total Production (Convention)
SrData-RAN1;19;36;2000-2030;IPSA00;ga_TotalProducti
PgNext-
PgName-International Petroleum Supply and Disposition (3 of 5)
PgSubT-(from Table 19)
GrNext-
GrName-Production, Unconventional
GrSubT-U.S. (50 States)
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-U.S. (50 states)
SrData-RAN1;19;39;2000-2030;IPSA00;ha_U.S.(50states
GrNext-
GrName-Production, Unconventional
GrSubT-Other North America
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Other North America
SrData-RAN1;19;40;2000-2030;IPSA00;ha_OtherNorthAme
GrNext-
GrName-Production, Unconventional
GrSubT-Western Europe
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Western Europe
SrData-RAN1;19;41;2000-2030;IPSA00;ha_WesternEurope
GrNext-
GrName-Production, Unconventional
GrSubT-Asia
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Asia
SrData-RAN1;19;42;2000-2030;IPSA00;ha_Asia         
GrNext-
GrName-Production, Unconventional
GrSubT-Middle East
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Middle East
SrData-RAN1;19;43;2000-2030;IPSA00;ha_MiddleEast   
GrNext-
GrName-Production, Unconventional
GrSubT-Africa
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Africa
SrData-RAN1;19;44;2000-2030;IPSA00;ha_Africa       
GrNext-
GrName-Production, Unconventional
GrSubT-South and Central America
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-South and Central America
SrData-RAN1;19;45;2000-2030;IPSA00;ha_SouthandCentr
GrNext-
GrName-Production, Unconventional
GrSubT-Total
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Total Production (Nonconve
SrData-RAN1;19;46;2000-2030;IPSA00;ha_TotalProducti
GrNext-
GrName-Total Production
GrSubT-Conventional and Unconventional
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Total Production
SrData-RAN1;19;48;2000-2030;IPSA00;ia_TotalProducti
PgNext-
PgName-International Petroleum Supply and Disposition (4 of 5)
PgSubT-(from Table 19)
GrNext-
GrName-Consumption - Industrialized Countries
GrSubT-U.S. (50 States)
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-U.S. (50 states)
SrData-RAN1;19;53;2000-2030;IPSA00;ka_U.S.(50states
GrNext-
GrName-Consumption - Industrialized Countries
GrSubT-U.S. Territories
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-U.S. Territories
SrData-RAN1;19;54;2000-2030;IPSA00;ka_U.S.Territori
GrNext-
GrName-Consumption - Industrialized Countries
GrSubT-Canada
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Canada
SrData-RAN1;19;55;2000-2030;IPSA00;ka_Canada       
GrNext-
GrName-Consumption - Industrialized Countries
GrSubT-Mexico
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Mexico
SrData-RAN1;19;56;2000-2030;IPSA00;ka_Mexico       
GrNext-
GrName-Consumption - Industrialized Countries
GrSubT-Western Europe
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Western Europe
SrData-RAN1;19;57;2000-2030;IPSA00;ka_WesternEurope
GrNext-
GrName-Consumption - Industrialized Countries
GrSubT-Japan
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Japan
SrData-RAN1;19;58;2000-2030;IPSA00;ka_Japan        
GrNext-
GrName-Consumption - Industrialized Countries
GrSubT-Australia and New Zealand
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Australia and New Zealand
SrData-RAN1;19;59;2000-2030;IPSA00;ka_AustraliaandN
GrNext-
GrName-Consumption - Industrialized Countries
GrSubT-Total
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Total Industrialized
SrData-RAN1;19;60;2000-2030;IPSA00;ka_TotalIndustri
GrNext-
GrName-Consumption - Eurasia
GrSubT-Former Soviet Union
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Former Soviet Union
SrData-RAN1;19;63;2000-2030;IPSA00;la_FormerSovietU
GrNext-
GrName-Consumption - Eurasia
GrSubT-Eastern Union
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Eastern Europe
SrData-RAN1;19;64;2000-2030;IPSA00;la_EasternEurope
GrNext-
GrName-Consumption - Eurasia
GrSubT-Total
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Total Eurasia
SrData-RAN1;19;65;2000-2030;IPSA00;la_TotalEurasia 
GrNext-
GrName-Consumption - Developing Countries
GrSubT-China
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-China
SrData-RAN1;19;68;2000-2030;IPSA00;ma_China        
PgNext-
PgName-International Petroleum Supply and Disposition (5 of 5)
PgSubT-(from Table 19)
GrNext-
GrName-Consumption - Developing Countries
GrSubT-India
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-India
SrData-RAN1;19;69;2000-2030;IPSA00;ma_India        
GrNext-
GrName-Consumption - Developing Countries
GrSubT-South Korea
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-South Korea
SrData-RAN1;19;70;2000-2030;IPSA00;ma_SouthKorea   
GrNext-
GrName-Consumption - Developing Countries
GrSubT-Other Asia
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Other Asia
SrData-RAN1;19;79;2000-2030;IPSA00;oa_OPECProductio
GrNext-
GrName-Consumption - Developing Countries
GrSubT-Middle East
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Middle East 7/
SrData-RAN1;19;72;2000-2030;IPSA00;ma_MiddleEast   
GrNext-
GrName-Consumption - Developing Countries
GrSubT-Africa
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Africa
SrData-RAN1;19;73;2000-2030;IPSA00;ma_Africa       
GrNext-
GrName-Consumption - Developing Countries
GrSubT-South and Central America
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-South and Central America
SrData-RAN1;19;74;2000-2030;IPSA00;ma_SouthandCentr
GrNext-
GrName-Consumption - Developing Countries
GrSubT-Total
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Total Developing Countries
SrData-RAN1;19;75;2000-2030;IPSA00;ma_TotalDevelopi
GrNext-
GrName-Consumption - Total
GrSubT-Million Barrels per Day
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Total Consumption
SrData-RAN1;19;77;2000-2030;IPSA00;na_TotalConsumpt
GrNext-
GrName-Opec Production
GrSubT-Million Barrels per Day
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-OPEC Production 10/
SrData-RAN1;19;79;2000-2030;IPSA00;oa_OPECProductio
GrNext-
GrName-Non-Opec Production
GrSubT-Million Barrels per Day
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Non-OPEC Production 10/
SrData-RAN1;19;80;2000-2030;IPSA00;oa_Non-OPECProdu
GrNext-
GrName-Net Eurasia Export
GrSubT-Million Barrels per Day
GrXLab-Year
GrYLab-mmBBL/day
SrNext-
SrName-Net Eurasia Exports
SrData-RAN1;19;81;2000-2030;IPSA00;oa_NetEurasiaExp
GrNext-
GrName-Opec Market Share
GrSubT-Fraction
GrXLab-Year
GrYLab-fraction
SrNext-
SrName-OPEC Market Share
SrData-RAN1;19;82;2000-2030;IPSA00;oa_OPECMarketSha
PgNext-
PgName-Light-Duty Vehicle Sales by Technology 
PgSubT-New Car Sales 
GrNext-
GrName-Gasoline ICE Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;4;2000-2030;TSTA00;ba_GasolineICEVe
GrNext-
GrName-TDI Diesel ICE Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;5;2000-2030;TSTA00;ba_TDIDieselICE 
GrNext-
GrName-Total Conventional Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;6;2000-2030;TSTA00;ba_TotalConventi
GrNext-
GrName-Methanol-Flex Fuel ICE Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;9;2000-2030;TSTA00;ca_Methanol-Flex
GrNext-
GrName-Methanol ICE  Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;10;2000-2030;TSTA00;ca_MethanolICE  
GrNext-
GrName-Ethanol-Flex Fuel ICE Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;11;2000-2030;TSTA00;ca_Ethanol-FlexF
GrNext-
GrName-Ethanol ICE Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;12;2000-2030;TSTA00;ca_EthanolICE   
GrNext-
GrName-Electric Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;13;2000-2030;TSTA00;ca_ElectricVehic
GrNext-
GrName-Electric-Diesel Hybrid Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;14;2000-2030;TSTA00;ca_Electric-Dies
GrNext-
GrName-Electric-Gasoline Hybrid Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;15;2000-2030;TSTA00;ca_Electric-Gaso
GrNext-
GrName-Compressed Natural Gas ICE Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;16;2000-2030;TSTA00;ca_CompressedNat
GrNext-
GrName-Compressed Natural Gas Bi-fuel Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;17;2000-2030;TSTA00;da_CompressedNat
PgNext-
PgName-Light-Duty Vehicle Sales by Technology
PgSubT-New Car  Sales
GrNext-
GrName-Liquefied Petroleum Gas ICE Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;18;2000-2030;TSTA00;da_LiquefiedPetr
GrNext-
GrName-Liquefied Petroleum Gas Bi-fuel Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;19;2000-2030;TSTA00;ea_LiquefiedPetr
GrNext-
GrName-Fuel Cell Gasoline Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;20;2000-2030;TSTA00;ea_FuelCellGasol
GrNext-
GrName-Fuel Cell Methanol Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;21;2000-2030;TSTA00;ea_FuelCellMetha
GrNext-
GrName-Fuel Cell Hydrogen Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;22;2000-2030;TSTA00;ea_FuelCellHydro
GrNext-
GrName-Total Alternative Cars
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;23;2000-2030;TSTA00;ea_TotalAlternat
GrNext-
GrName-Percent Alternative Car Sales
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;25;2000-2030;TSTA00;fa_PercentAltern
GrNext-
GrName-Total New Car Sales
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;26;2000-2030;TSTA00;fa_TotalNewCarSa
PgNext-
PgName-Light-Duty Vehicle Sales by Technology 
PgSubT-New Light Truck Sales 
GrNext-
GrName-Gasoline ICE Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;30;2000-2030;TSTA00;ga_GasolineICEVe
GrNext-
GrName-TDI Diesel ICE Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;31;2000-2030;TSTA00;ga_TDIDieselICE 
GrNext-
GrName-Total Conventional Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;32;2000-2030;TSTA00;ga_TotalConventi
GrNext-
GrName-Methanol-Flex Fuel ICE Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;35;2000-2030;TSTA00;ha_Methanol-Flex
GrNext-
GrName-Methanol ICE Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;36;2000-2030;TSTA00;ha_MethanolICE  
GrNext-
GrName-Ethanol-Flex Fuel ICE Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;37;2000-2030;TSTA00;ha_Ethanol-FlexF
GrNext-
GrName-Ethanol ICE Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;38;2000-2030;TSTA00;ha_EthanolICE   
GrNext-
GrName-Electric Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;39;2000-2030;TSTA00;ha_ElectricVehic
GrNext-
GrName-Electric-Diesel Hybrid Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;40;2000-2030;TSTA00;ha_Electric-Dies
GrNext-
GrName-Electric-Gasoline Hybrid Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;41;2000-2030;TSTA00;ha_Electric-Gaso
GrNext-
GrName-Compressed Natural Gas ICE Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;42;2000-2030;TSTA00;ha_CompressedNat
GrNext-
GrName-Compressed Natural Gas Bi-fuel Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;43;2000-2030;TSTA00;ia_CompressedNat
PgNext-
PgName-Light-Duty Vehicle Sales by Technology 
PgSubT-New Light Truck Sales 
GrNext-
GrName-Liquefied Petroleum Gas ICE Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;44;2000-2030;TSTA00;ia_LiquefiedPetr
GrNext-
GrName-Liquefied Petroleum Gas Bi-fuel Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;45;2000-2030;TSTA00;ja_LiquefiedPetr
GrNext-
GrName-Fuel Cell Gasoline Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;46;2000-2030;TSTA00;ja_FuelCellGasol
GrNext-
GrName-Fuel Cell Methanol Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;47;2000-2030;TSTA00;ja_FuelCellMetha
GrNext-
GrName-Fuel Cell Hydrogen Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;48;2000-2030;TSTA00;ja_FuelCellHydro
GrNext-
GrName-Total Alternative Lt. Trucks
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;49;2000-2030;TSTA00;ja_TotalAlternat
GrNext-
GrName-Percent Alternative L.T. Sales
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;51;2000-2030;TSTA00;ka_PercentAltern
GrNext-
GrName-Total New Truck Sales
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;52;2000-2030;TSTA00;ka_TotalNewTruck
PgNext-
PgName-Light-Duty Vehicle Sales by Technology 
PgSubT-Total Cars and Lt. Trucks
GrNext-
GrName-Percent Total Alternative Sales
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;54;2000-2030;TSTA00;la_PercentTotalA
GrNext-
GrName-EPACT Legislative  Alternative Sales
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;55;2000-2030;TSTA00;la_EPACTLegislat
GrNext-
GrName-ZEVP Legislative Alternative Sales
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;56;2000-2030;TSTA00;la_ZEVPLegislati
GrNext-
GrName-Total Vehicles Sales
GrSubT-Thousands
GrXLab-Years
GrYLab-Thous.
SrNext-
SrName-Place Holder
SrData-RAN1;47;58;2000-2030;TSTA00;ma_TotalVehicles
PgNext-
PgName-National Impacts of the Clean Air Act Amendments of 1990  (Ftab Table 117)
PgSubT-Page 1 of 4
GrNext-
GrName-Nitrogen Oxides
GrSubT-million tons
GrXLab-Years
GrYLab-mill. tons
SrNext-
SrName-Place Holder
SrData-RAN1;107;3;2000-2030;CAAA00;ba_NitrogenOxide
GrNext-
GrName-Sulfur Dioxide (million tons)
GrSubT-
GrXLab-Years
GrYLab-mill. tons
SrNext-
SrName-Place Holder
SrData-RAN1;107;4;2000-2030;CAAA00;ba_SulfurDioxide
GrNext-
GrName-Sulfur Dioxide From Coal
GrSubT-million tons
GrXLab-Years
GrYLab-mill. tons
SrNext-
SrName-Place Holder
SrData-RAN1;107;5;2000-2030;CAAA00;ba_FromCoal     
GrNext-
GrName-Sulfur Dioxide from Oil/Other
GrSubT-million tons
GrXLab-Years
GrYLab-mill. tons
SrNext-
SrName-Place Holder
SrData-RAN1;107;6;2000-2030;CAAA00;ba_Oil/Other    
GrNext-
GrName-Mercury
GrSubT-tons
GrXLab-Years
GrYLab-tons
SrNext-
SrName-Place Holder
SrData-RAN1;107;7;2000-2030;CAAA00;ba_Mercury(tons)
GrNext-
GrName-Carbon Dioxide
GrSubT-million metric tons
GrXLab-Years
GrYLab-mmtco2
SrNext-
SrName-Place Holder
SrData-RAN1;107;8;2000-2030;CAAA00;ba_CarbonDioxide
GrNext-
GrName-Nox Allowance Price- Regional/Seasonal
GrSubT-$2003/ton
GrXLab-Years
GrYLab-$/ton
SrNext-
SrName-Place Holder
SrData-RAN1;107;12;2000-2030;CAAA00;ca_Regional/Seas
GrNext-
GrName-East/Annual
GrSubT-$2003/ton
GrXLab-Years
GrYLab-$/ton
SrNext-
SrName-Place Holder
SrData-RAN1;107;13;2000-2030;CAAA00;ca_East/Annual  
GrNext-
GrName-West/Annual
GrSubT-$2003/ton
GrXLab-Years
GrYLab-$/ton
SrNext-
SrName-Place Holder
SrData-RAN1;107;14;2000-2030;CAAA00;ca_West/Annual  
GrNext-
GrName-Allowance Price--SO2--CMM East
GrSubT-$2003/ton
GrXLab-Years
GrYLab-$2003/ton
SrNext-
SrName-Place Holder
SrData-RAN1;107;16;2000-2030;CAAA00;ca_CMMEast      
GrNext-
GrName-Allowance Price--SO2--EMM East
GrSubT-$2003/ton
GrXLab-Years
GrYLab-$2003/ton
SrNext-
SrName-Place Holder
SrData-RAN1;107;17;2000-2030;CAAA00;ca_EMMEast      
GrNext-
GrName-Mercury Allowance Price
GrSubT-(million 2003 $/ton)
GrXLab-Years
GrYLab-mill.$/ton
SrNext-
SrName-(million 2003 $/ton)
SrData-RAN1;107;21;2000-2030;CAAA00;ca_(million2003$
PgNext-
PgName-National Impacts of the Clean Air Act Amendments of 1990 (Ftab Table 117)
PgSubT-Page 2 of 4
GrNext-
GrName-Mercury Allowance Price
GrSubT-thousand 2003 $/lb
GrXLab-Years
GrYLab-th.$/lb
SrNext-
SrName-(thousand 2003 $/lb)
SrData-RAN1;107;22;2000-2030;CAAA00;ca_(thousand2003
GrNext-
GrName-Mercury Allowance Price--EMM
GrSubT-thousand 2003 $/lb
GrXLab-Years
GrYLab-th.$/lb
SrNext-
SrName-EMM (thousand 2003 $/lb)
SrData-RAN1;107;23;2000-2030;CAAA00;ca_EMM(thousand#
GrNext-
GrName-Carbon Dioxide Allowance Price
GrSubT-(2003 $/mmtco2e)
GrXLab-Years
GrYLab-$/mmtco2
SrNext-
SrName-Carbon Dioxide(2003 $/mmtco2e)
SrData-RAN1;107;24;2000-2030;CAAA00;ca_CarbonDioxide
GrNext-
GrName-Retrofits--Scrubber--Planned
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Planned
SrData-RAN1;107;28;2000-2030;CAAA00;da_Planned      
GrNext-
GrName-Retrofits--Scrubber--Unplanned
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Unplanned
SrData-RAN1;107;29;2000-2030;CAAA00;da_Unplanned    
GrNext-
GrName-Retrofits--Scrubber--Total
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Total
SrData-RAN1;107;30;2000-2030;CAAA00;da_Total        
GrNext-
GrName-NOX Controls--Combusion--Planned
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Planned
SrData-RAN1;107;33;2000-2030;CAAA00;ea_Planned      
GrNext-
GrName-NOX Controls--Combusion--Unplanned
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Unplanned
SrData-RAN1;107;34;2000-2030;CAAA00;ea_Unplanned    
GrNext-
GrName-NOX Controls--Combusion--Total
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Total
SrData-RAN1;107;35;2000-2030;CAAA00;ea_Total        
GrNext-
GrName-NOX Controls--SCR Post-Combustion--Planned
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Planned
SrData-RAN1;107;37;2000-2030;CAAA00;fa_Planned      
GrNext-
GrName-NOX Controls--SCR Post-Combustion--Unplanned
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Unplanned
SrData-RAN1;107;38;2000-2030;CAAA00;fa_Unplanned    
GrNext-
GrName-NOX Controls--SCR Post-Combustion--Total
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Total
SrData-RAN1;107;39;2000-2030;CAAA00;fa_Total        
PgNext-
PgName-National Impacts of the Clean Air Act Amendments of 1990 (Ftab Table 117)
PgSubT-Page 3 of 4
GrNext-
GrName-NOX Controls--SNCR Post-Combustion--Planned
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Planned
SrData-RAN1;107;41;2000-2030;CAAA00;ga_Planned      
GrNext-
GrName-NOX Controls--SNCR Post-Combustion--Unplanned
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Unplanned
SrData-RAN1;107;42;2000-2030;CAAA00;ga_Unplanned    
GrNext-
GrName-NOX Controls--SNCR Post-Combustion--Total
GrSubT-gigawatts
GrXLab-Years
GrYLab-GW
SrNext-
SrName-Total
SrData-RAN1;107;43;2000-2030;CAAA00;ga_Total        
GrNext-
GrName-Coal Production--Low Sulfur (< .61 lbs/mmBtu)
GrSubT-million tons
GrXLab-Years
GrYLab-mill.tons
SrNext-
SrName-Low Sulfur (< .61 lbs/mmBtu)
SrData-RAN1;107;47;2000-2030;CAAA00;ha_LowSulfur(<.6
GrNext-
GrName-Coal Production--Medium Sulfur
GrSubT-million tons
GrXLab-Years
GrYLab-mill.tons
SrNext-
SrName-Medium Sulfur
SrData-RAN1;107;48;2000-2030;CAAA00;ha_MediumSulfur 
GrNext-
GrName-Coal Production--High Sulfur (> 1.67 lbs/mmBtu)
GrSubT-million tons
GrXLab-Years
GrYLab-mill.tons
SrNext-
SrName-High Sulfur (> 1.67 lbs/mmBtu)
SrData-RAN1;107;49;2000-2030;CAAA00;ha_HighSulfur(>1
GrNext-
GrName-Interregional SO2 Permits--Target
GrSubT-million tons
GrXLab-Years
GrYLab-mill.tons
SrNext-
SrName-Target (millions tons)
SrData-RAN1;107;51;2000-2030;CAAA00;ha_Target(millio
GrNext-
GrName-Interregional SO2--Cumulative Banked Permits
GrSubT-million tons
GrXLab-Years
GrYLab-mill.tons
SrNext-
SrName-Cumulative Banked Allowances
SrData-RAN1;107;54;2000-2030;CAAA00;ha_CumulativeBan
GrNext-
GrName-Coal SO2 Content
GrSubT-lbs/mmbtu
GrXLab-Years
GrYLab-(lbs/mmbtu)
SrNext-
SrName-SO2 Content (lbs/mmbtu)
SrData-RAN1;107;59;2000-2030;CAAA00;ja_SO2Content(lb
GrNext-
GrName-Coal Mercury Content
GrSubT-(lbs/trill.btu)
GrXLab-Years
GrYLab-lbs/trill
SrNext-
SrName-Mercury Content (lbs/mmmmbtu)
SrData-RAN1;107;60;2000-2030;CAAA00;ja_MercuryConten
GrNext-
GrName-ACI Controls--Spray Cooling
GrSubT-units
GrXLab-Years
GrYLab-units
SrNext-
SrName-Spray Cooling
SrData-RAN1;107;63;2000-2030;CAAA00;ka_SprayCooling 
GrNext-
GrName-ACI Controls--Supplemental Fabric Filter
GrSubT-units
GrXLab-Years
GrYLab-units
SrNext-
SrName-Supplemental Fabric Filter
SrData-RAN1;107;64;2000-2030;CAAA00;ka_SupplementalF
PgNext-
PgName-National Impacts of the Clean Air Act Amendments of 1990 (Ftab Table 117)
PgSubT-Page 4 of 4
GrNext-
GrName-ACI Mercury Removal
GrSubT-tons
GrXLab-Years
GrYLab-tons
SrNext-
SrName-ACI Mercury Removal (tons)
SrData-RAN1;107;66;2000-2030;CAAA00;la_ACIMercuryRem
GrNext-
GrName-Allowance Revenue--Nitrogen Oxides
GrSubT-billion 2003 dollars
GrXLab-Years
GrYLab-bill. $
SrNext-
SrName-Nitrogen Oxides
SrData-RAN1;107;69;2000-2030;CAAA00;ma_NitrogenOxide
GrNext-
GrName-Allowance Revenue--Sulfur Dioxide
GrSubT-billion 2003 dollars
GrXLab-Years
GrYLab-bill. $
SrNext-
SrName-Sulfur Dioxide
SrData-RAN1;107;70;2000-2030;CAAA00;ma_SulfurDioxide
GrNext-
GrName-Allowance Revenue--Mercury
GrSubT-billion 2003 dollars
GrXLab-Years
GrYLab-bill. $
SrNext-
SrName-Mercury
SrData-RAN1;107;71;2000-2030;CAAA00;ma_Mercury      
GrNext-
GrName-Allowance Revenue--Carbon
GrSubT-billion 2003 dollars
GrXLab-Years
GrYLab-bill. $
SrNext-
SrName-Carbon
SrData-RAN1;107;72;2000-2030;CAAA00;ma_Carbon       
GrNext-
GrName-Allowance Revenue--Total
GrSubT-billion 2003 dollars
GrXLab-Years
GrYLab-bill. $
SrNext-
SrName-Total
SrData-RAN1;107;73;2000-2030;CAAA00;ma_Total        
Finish-
